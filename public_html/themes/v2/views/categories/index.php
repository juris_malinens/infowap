<?php
/*$this->breadcrumbs=array(
	'Categories',
);*/

if(Yii::app()->getModule('user')->isAdmin()) {
    $this->menu=array(
            array('label'=>'Create Categories', 'url'=>array('create')),
            array('label'=>'Manage Categories', 'url'=>array('admin')),
    );
}
?>

<!--<p>Active sites: <?=$active_sites?></p>-->
<ul>
<?php foreach($categories as $nId => $category): ?>
    
  <li class="grid_4">
    <div class="title_bg">
      <div class="title">
        <h2><?=CHtml::link(Yii::t('infowap', $category['name']).' ['.$category['count'].']',
    array('/'.$category['link']))?></h2>
      </div>
    </div>
    <ul class="links clearfix">
<?php
$sites = Yii::app()->db->createCommand()
    ->select('sites.*,
            SUM(statistics.in) AS sum_in,
            SUM(statistics.out) sum_out,
            MAX(statistics.date) AS max_date')
    ->from('tbl_sites sites')
    ->join('tbl_categories categories', 'sites.category_id=categories.id
        AND categories.link = :link', array(':link'=>$category['link']))
    ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
        AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
    ->where('sites.is_banned = 0')
    ->group('sites.id')
    ->having('SUM(statistics.in) > 0')
    ->order('sum_in DESC, statistics.date DESC')
    ->limit(10)
    ->queryAll();
?>

<?php foreach($sites as $nId2 => $site): ?>
    <?php $sites[$nId2]['seo_link'] = $this->to_seo($site['title']); ?>
<?php endforeach; ?>
    
<?php foreach($sites as $site): ?>
<li>
<?=CHtml::link(
    str_replace('http://','', $site['wap_link']).' ['.$site['sum_in'].'/'.$site['sum_out'].']',
    array('/sites/info/'.$site['id'].'-'.$site['seo_link']),
    array('title'=>htmlspecialchars($site['title']))
)?>
</li>
<?php endforeach; ?>

    </ul>
    <div class="clear"></div>
  </li>

<?php if($nId != 2 && $nId != 5 && $nId != 8): ?>
  <li class="grid_2"></li>
<?php elseif($nId == 2 || $nId == 5 || $nId == 8): ?>
  <div class="clearfix"></div>
<?php endif; ?>

<?php endforeach; ?>
</ul>