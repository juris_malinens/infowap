<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />

<link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl?>/css/reset.css" />
<link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl?>/css/text.css" />
<link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl?>/css/960.css" />
<link rel="stylesheet" href="<?=Yii::app()->theme->baseUrl?>/css/infowap.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="wrapper">
<div id="top_line" class="container_16 clearfix">
  <div class="clear"></div>
</div><!-- end .container_16 or #top_line -->

<div id="top" class="container_16 clearfix">
<a style="float: left;" href="http://<?=$_SERVER['SERVER_NAME']?>">Back to main page</a>
    Mobile websites for all needs. <a href="http://m.infowap.info<?=$_SERVER['REQUEST_URI']?>">Switch to mobile version</a><!-- <?php echo CHtml::encode(Yii::app()->name); ?> -->
  <div class="clear"></div>

</div><!-- end .container_16 or #top -->

<div class="container_16 clearfix">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-2212583322739900";
/* www.infowap.info leader */
google_ad_slot = "2076012613";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div><!-- end .container_16 of google -->

<div id="content" class="container_16 clearfix">

	<div id="mainmenu">
		<?php /*$this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('post/index')),
				//array('label'=>'About', 'url'=>array('site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('site/contact')),
				array('label'=>'Login', 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		));*/ ?>
	</div><!-- mainmenu -->

	<?php /*$this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	));*/ ?><!-- breadcrumbs -->

	<?php echo $content; ?>

<div id="login" class="container_16 clearfix">
  <div class="grid_16 login">
    <form id="login-form" method="post" action="/site/login">
      <div class="title_bg">
          <input class="title" name="login" value="" type="text" placeholder="login..."/>
      </div>
      <div class="title_bg">
        <input class="title" name="password" value="" type="password" placeholder="password..."/>
      </div>
      <div class="title_bg">
        <input class="title" value="Manage sites" type="submit"/>
      </div>
      <div class="title_bg" style="float: right;margin-right: 0px;">
        <div class="title register_title">
          <?=CHtml::link('Create account', array('user/registration'), array('class'=>'register'))?>
        </div>
      </div>
    </form>
  </div>

</div><!-- end .container_16 or #top -->
        
</div>

</div><!-- end .wrapper -->
<footer>
  <div class="grid_16 footer clearfix" id="footer_line">
  </div><!-- end .grid_16 #footer_line -->
  <div class="clear"></div>
  <div class="grid_16 clearfix" id="footer">
    (c) <?=date('Y')?>
  </div><!-- end .grid_16 #footer_line -->
  <div class="clear"></div>
</footer>
</body>
</html>