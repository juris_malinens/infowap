<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mobile.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name ="viewport" content ="width = device-width, height = device-height"/>
</head>
<body>
<div class="container" id="page">
<script type="text/javascript">
 var inmobi_conf = {
 siteid : "4028cb902494cc120124b3c9de8e01d4",
 slot : "15"
};
</script><script type="text/javascript" src="http://cf.cdn.inmobi.com/ad/inmobi.js"></script>
<?php
if(Yii::app()->session['wurfl']['is_smartphone']):
?>
<div id="inmobi"></div>
<div class="clearfix"></div>
<?php
else:
?>
<?php endif; ?>


	<div id="header">
		<div id="logo">
                    <h1><?=CHtml::encode(Yii::app()->name)?></h1>
                    <!--<span class="beta">[BETA]</span>-->
                </div>
	</div><!-- header -->

	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'homeLink'=>CHtml::link('Categories', array('categories/')),
		'links'=>$this->breadcrumbs
	)); ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="mainmenu">
            <h3>Site owners area:</h3>
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/categories')),
				//array('label'=>'About', 'url'=>array('site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('site/contact')),
				/*array('label'=>'Manage Your sites', 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
                                array('label'=>'Register to add WAP sites', 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('site/logout'), 'visible'=>!Yii::app()->user->isGuest),*/
array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Manage Your sites"), 'visible'=>Yii::app()->user->isGuest),
array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Create account"), 'visible'=>Yii::app()->user->isGuest),
array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Profile"), 'visible'=>!Yii::app()->user->isGuest),
array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->
        
	<footer>
            <ul>
                <li class="first last center">
                    &copy; <?php echo date('Y'); ?> by Infowap
                </li>
            </ul>
                <!--<span class="facebook"></span>-->
	</footer><!-- footer -->

</div><!-- page -->

</body>
</html>