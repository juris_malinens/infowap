<?php

class CategoriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
    
    /**
     * 
     * @assert ('Test') === 'test'
     * @assert ('Test#') === 'test'
     * @param string $sText
     * @return string
     */
    public function to_seo($sText)
    {

        $sText = mb_strtolower($sText);
        $sText = preg_replace('/[^a-z0-9-]/', '-', $sText);
        $sText = preg_replace('/-+/', "-", $sText);

        /**
         * Additional conversation to languages other than Russian, Latvian,
         * Lithuanian
         */
        $sText = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $sText);

        $sText = trim($sText, "-");
        return $sText;
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'category', 'all'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Categories;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
         * @todo needs work
	 */
	public function actionIndex()
	{
            
            //selecte categories and sites count with at least one
            // in hit registered in last 7 days
            $categories = Yii::app()->db->createCommand()
                ->select('
                    t.*,
                    COUNT(DISTINCT statistics.site_id) AS count,
                    SUM(statistics.in) AS sum_in,
                    SUM(statistics.out) AS sum_out')
                ->from('tbl_categories t')
                ->leftJoin('tbl_sites sites', 'sites.category_id=t.id AND sites.is_banned = 0')
                ->leftJoin('tbl_statistics statistics',
                'statistics.site_id=sites.id AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND statistics.in > 0')
                ->group('t.id')
                ->queryAll();
            
            /*$categories = Yii::app()->db->createCommand()
                ->select('sites.*,
                        SUM(statistics.in) AS sum_in,
                        SUM(statistics.out) sum_out,
                        MAX(statistics.date) AS max_date')
                ->from('tbl_sites sites')
                ->join('tbl_categories categories', 'sites.category_id=categories.id
                    AND categories.link = :link', array(':link'=>$category))
                ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
                    AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->group('sites.id')
                ->order('sum_in, statistics.date DESC')*/
            
            $nActiveSites = 0;
            if($categories) {
                foreach($categories as $category)
                    $nActiveSites += $category['count'];
            }

            $this->render('index', array(
                'categories' => $categories,
                'active_sites' => $nActiveSites
                ));

	}

	/**
	 * Lists all sites
         * @todo needs work
	 */
	public function actionAll($page = 0)
	{
            $count = Yii::app()->db->createCommand()
                ->select('sites.id')
                ->from('tbl_sites sites')
                ->join('tbl_categories categories', 'sites.category_id=categories.id')
                ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
                    AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->where('sites.is_banned = 0')
                ->group('sites.id')
                ->having('SUM(statistics.in) > 0')
                ->queryAll();
            $count = count($count);

            $pages=new CPagination($count);
            // results per page
            $pages->pageSize=12;

            $sites = Yii::app()->db->createCommand()
                ->select('sites.*,
                        SUM(statistics.in) AS sum_in,
                        SUM(statistics.out) sum_out,
                        MAX(statistics.date) AS max_date')
                ->from('tbl_sites sites')
                ->join('tbl_categories categories', 'sites.category_id=categories.id')
                ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
                    AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->where('sites.is_banned = 0')
                ->group('sites.id')
                ->having('SUM(statistics.in) > 0')
                ->order('sum_in DESC, statistics.date DESC')
                ->offset(($page-1)*$pages->pageSize)
                ->limit($pages->pageSize)
                ->queryAll();

            foreach ($sites as $nId => $site) {
                $sites[$nId]['seo_link'] = $this->to_seo($site['title']);
            }

            $this->setPageTitle('Mobile sites TOP list');
            
            $this->render('categories/all', array(
                'sites' => $sites,
                'pages' => $pages,
                'active_sites' => $count
            ));

	}

	/**
	 * Lists all models.
         * @todo needs work
	 */
	public function actionCategory($category, $page = 0)
	{
            $count = Yii::app()->db->createCommand()
                ->select('sites.id')
                ->from('tbl_sites sites')
                ->join('tbl_categories categories', 'sites.category_id=categories.id
                    AND categories.link = :link', array(':link'=>$category))
                ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
                    AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->where('sites.is_banned = 0')
                ->group('sites.id')
                ->having('SUM(statistics.in) > 0')
                ->queryAll();
            $count = count($count);

            $pages=new CPagination($count);
            // results per page
            $pages->pageSize=6;
            
            $sites = Yii::app()->db->createCommand()
                ->select('sites.*,
                        SUM(statistics.in) AS sum_in,
                        SUM(statistics.out) sum_out,
                        MAX(statistics.date) AS max_date')
                ->from('tbl_sites sites')
                ->join('tbl_categories categories', 'sites.category_id=categories.id
                    AND categories.link = :link', array(':link'=>$category))
                ->leftJoin('tbl_statistics statistics', 'statistics.site_id=sites.id
                    AND statistics.date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->where('sites.is_banned = 0')
                ->group('sites.id')
                ->having('SUM(statistics.in) > 0')
                ->order('sum_in DESC, statistics.date DESC')
                ->offset(($page-1)*$pages->pageSize)
                ->limit($pages->pageSize)
                ->queryAll();

            foreach($sites as $nId => $site)
                $sites[$nId]['seo_link'] = $this->to_seo($site['title']);

            $category = Categories::model()->find('link=:link', array(':link'=>$category));
            
            $this->setPageTitle($category->name.'- list of WAP sites');
            
            $this->render('category', array(
                'sites' => $sites,
                'pages' => $pages,
                'active_sites' => $count,
                'category' => $category
            ));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Categories('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Categories']))
			$model->attributes=$_GET['Categories'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Categories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
