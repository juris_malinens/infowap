<?php
/**
 * class to register in/out clicks aaaaand apply security checks
 * @since 2012.04.02
 * @version 0.7
 * @author Juris Malinens <juris.malinens@inbox.lv>
 */
class ClicksController extends Controller {
    
    public $is_opera_mini;
    
    /**
     * get real user agent
     * @return string 
     */
    private function ua() {
        
        //UCWEB?
        $sUserAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        if (stristr($sUserAgent, 'Opera Mini')) {
            
            $this->is_opera_mini = 1;
            if (isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']))
                $browser = addslashes(strip_tags($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']));
            else
                $browser = addslashes(strip_tags($sUserAgent));
            
        } else {
            
            $this->is_opera_mini = 0;
            $browser = addslashes(strip_tags($sUserAgent));
            
        }
        
        return $browser;
        
    }
    
    /**
     * get real IP
     * @return string 
     */
    function ip() {
        
        if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            
            if(strpos($_SERVER['HTTP_FORWARDED_FOR'], ',') !== FALSE) {
                
                $aIPs = explode(',', $_SERVER['HTTP_FORWARDED_FOR']);
                
                foreach($aIPs as $sIP) {
                    
                    if(substr($sIP, 0, 3) != '192' && substr($sIP, 0, 3) != '127') {
                        
                        $ip = $sIP;
                        break;
                        
                    }
                    
                }
                
                if(!isset($ip))
                    $ip = $_SERVER['REMOTE_ADDR'];
                
            } else
                $ip = $_SERVER['HTTP_FORWARDED_FOR'];
            
        } else
            $ip = $_SERVER['REMOTE_ADDR'];
        
        return $ip;
        
    }
    
    
    /**
     * checks if IP is a proxy server
     * @assert ('77.93.13.41') === FALSE
     * @param int $ip - IP address
     */
    private function is_ip_blocked($ip) {
        
        $dnsbl_lists = array("proxies.dnsbl.sorbs.net");
        $reverse_ip = implode(".", array_reverse(explode(".", $ip))); 
        foreach ($dnsbl_lists as $dnsbl_list)
            if (checkdnsrr($reverse_ip . "." . $dnsbl_list . ".", "A"))
                return TRUE;
            
        return FALSE;
        
    }
    
    /**
     * first entrance where cookie is set to try to fight illegitime clicks
     * @param int $id 
     */
    public function actionId($id) {
        
        $sReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
        Yii::app()->session['referer'] = $sReferer;
        
        $sCookieName = md5(date("W") . $id . 'first_pass');
        
        //set cookie for 60 seconds to check in second page
        Yii::app()->request->cookies[$sCookieName] = new CHttpCookie($sCookieName, '1', array('expire' => time() + 60));
        
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        echo "<!DOCTYPE html>
                <html>
                <head>
                    <title>Page is loading...</title>
                    <meta http-equiv=\"refresh\" content=\"1;url=/clicks/enter/id/$id\"/>
                </head>
                <body>
                <div>
                    <a href=\"/clicks/enter/id/$id\">Proceed manually...</a>
                </div>
                </body>
                </html>";
        exit;
    }

    /**
     * 2nd entrance page where click is checked if it is good and saved to DB
     * @param int $id 
     */
    public function actionEnter($id) {

        $sCookieName = md5(date("W") . $id);
        $sCookieNameFirstPass = md5(date("W") . $id . 'first_pass');
        
        $ip = $this->ip();
        $ua = $this->ua();
        $bIsIpBlocked = (int)$this->is_ip_blocked($ip);
        
        
        $sCountryCode = !empty($_SERVER['GEOIP_COUNTRY_CODE']) ? $_SERVER['GEOIP_COUNTRY_CODE'] : 
            (!empty($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] :'US');
        $sContinentCode = $sRegion = $sCity = $sOrganization = '';
        
        if (function_exists('geoip_continent_code_by_name')) {
            $sContinentCode = geoip_continent_code_by_name($ip);
            $sCountryCode = geoip_country_code_by_name($ip);
            $sRegion = isset($_SERVER["GEOIP_REGION_NAME"]) ? $_SERVER["GEOIP_REGION_NAME"] : '';
            $aGeoIP = geoip_record_by_name($ip);
            $sCity = $aGeoIP['city'];
            $sOrganization = geoip_org_by_name($ip);
        }

        
        $sReferer = isset(Yii::app()->session['referer']) ? Yii::app()->session['referer'] : "";
        
        /*
         * check if ID exists and is not banned
         */
        $oSite = Sites::model()->find('id=:id', array(':id' => $id));
        if (!$oSite) {
            $sWarning = "Site not found. id=".$id;
            Yii::log($sWarning, 'warning', 'public_html.controllers.ClicksController');
        }

        if ($oSite->is_banned) {
            $sWarning = "Site is banned";
            Yii::log($sWarning, 'warning', 'public_html.controllers.ClicksController');
        }

        /**
         * check if this IP and browser has already clicked link today
         * check DB + check COOKIE
         */
        $oClickExists = Clicks::model()
                ->find('user_agent=:user_agent AND ip=:ip AND DATE_FORMAT(datetime, \'%Y-%m-%d\')=:date', array(
            ':user_agent' => $ua,
            ':ip' => $ip,
            ':date' => date("Y-m-d")));

        if (isset(Yii::app()->request->cookies[$sCookieName])) {
            $bHasCookieClick = TRUE;
            $sWarning = 'cookie jau ir';
            Yii::log($sWarning, 'warning', 'public_html.controllers.ClicksController');
        } else {
            $bHasCookieClick = FALSE;
            Yii::app()->request->cookies[$sCookieName] = new CHttpCookie($sCookieName, '1', array('expire' => strtotime("next Monday")));
            $sInfo = 'cookie vēl nav. viss OK';
            Yii::log($sInfo, 'info', 'public_html.controllers.ClicksController');
        }

        $bHasCookieFirstPass = isset(Yii::app()->request->cookies[$sCookieNameFirstPass]) ? true : false;

        /**
         * add to tbl_clicks
         */
        if ($oSite && $bHasCookieFirstPass && !$bHasCookieClick && !$oSite->is_banned) {
            
            $oClicks = new Clicks;
            $oClicks->site_id = $oSite->id;
            $oClicks->datetime = new CDbExpression('NOW()');
            $oClicks->user_agent = $ua;
            $oClicks->ip = $ip;
            $oClicks->continent = $sContinentCode;
            $oClicks->country = $sCountryCode;
            $oClicks->region = $sRegion;
            $oClicks->city = $sCity;
            $oClicks->organization = $sOrganization;
            $oClicks->is_opera_mini = $this->is_opera_mini;
            $oClicks->referer = $sReferer;
            $oClicks->is_ip_blocked = $bIsIpBlocked;
            
            if (!$oClickExists && !$bIsIpBlocked)
                $oClicks->is_click_good = 1;
            else
                $oClicks->is_click_good = 0;
            if(!$oClicks->save()) {

                $sWarning = 'problēmas saglabāt click datus: '.implode(" | ", $oClicks->getErrors());
                Yii::log($sWarning, 'warning', 'public_html.controllers.ClicksController');
            
            }
            
            //update tbl_statistics if it is new and good click
            if (!$oClickExists && !$bIsIpBlocked) {

                $oStatisticsExists = Statistics::model()->find(
                        'date=:date AND site_id=:site_id', array(
                    //':date'=>new CDbExpression('CURDATE()'),
                    ':date' => date("Y-m-d"),
                    ':site_id' => $id
                        ));
                
                if ($oStatisticsExists) {
                    //update
                    $oStatisticsExists->in += 1;
                    $oStatisticsExists->save();
                } else {
                    //insert
                    $oStatistics = new Statistics;
                    $oStatistics->site_id = $oSite->id;
                    //$oStatistics->date = new CDbExpression('CURDATE()');
                    $oStatistics->date = date("Y-m-d");
                    $oStatistics->in = 1;
                    $oStatistics->out = 0;
                    $oStatistics->save();
                }
            }
        }

        /**
         * redirect to category 
         */
        if ($oSite) {
            
            $oCategory = Categories::model()->find('id=:id', array(':id' => $oSite->category_id));
            
        }
        $this->redirect('/');
    }
    
    /**
     * register outgoing clicks
     * @param int $id
     * @param string $type - wap or web 
     */
    public function actionOut($id, $type) {

        $oStatisticsExists = Statistics::model()->find(
            'date=:date AND site_id=:site_id', array(
            ':date' => date("Y-m-d"),
            ':site_id' => $id
        ));
        
        $oSite = Sites::model()->find('id=:id', array(':id' => $id));
        if ($oSite) {
            
            if(stripos($oSite->title.$oSite->wap_link.$oSite->web_link, 'dcp') !== false) {
                $this->redirect('http://bwap.org');
            }
            
            if ($oStatisticsExists) {
                //update
                $oStatisticsExists->out += 1;
                $oStatisticsExists->save();
            } else {
                //insert
                $oStatistics = new Statistics;
                $oStatistics->site_id = $oSite->id;
                $oStatistics->date = date("Y-m-d");
                $oStatistics->in = 0;
                $oStatistics->out = 1;
                $oStatistics->save();
            }
            
            if($type == 'web')
                $this->redirect($oSite->web_link);
            else
                $this->redirect($oSite->wap_link);
        }
    }



    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}