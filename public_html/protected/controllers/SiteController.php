<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

/**
 *  
 */
function actionRemind() {

}

/**
 * 
 */
function actionMailsend() {
    
    set_time_limit(0);
    
    $dbC = Yii::app()->db->createCommand();
    $dbC->select()->from('tbl_emails')->where('sent = 0');

    foreach ($dbC->queryAll() as $row) {
        
        $headers = "MIME-Version: 1.0\r\nFrom: infowap@infowap.info\r\nReply-To: infowap@infowap.info\r\nContent-Type: text/html; charset=utf-8";
        $message = "Hi! <br/>\r\n
            You have already registered on http://infowap.info with WAP site: ".$row['site']." <br/>\r\n
            We have updated toplist but old registrations have been deleted.<br/>\r\n
            Please resubmit Your sites to http://infowap.info <br/>\r\n
            Please read rules before submitting!<br/>\r\n
            <br/>\r\n
            <br/>\r\n
            Infowap administration
            ";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        $subject = "Add Your mobile website to Infowap";
        $ok = @mail($row['email'],'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
        if(!$ok) {
            
            echo "<span style=\"color:red;\">".$row['email']." ".$row['site']." failed</span>";
            Yii::app()->db->createCommand()->update('tbl_emails', array(
                'sent'=>1,
                'failed'=>1,
            ), 'email=:email', array(':email'=>$row['email']));
            
        } else {
            
            echo "<span style=\"color:green;\">".$row['email']." ".$row['site']." OK</span>";
            Yii::app()->db->createCommand()->update('tbl_emails', array(
                'sent'=>1,
                'failed'=>0,
            ), 'email=:email', array(':email'=>$row['email']));
            
        }
        
        sleep(1);
        
    }
    
}

/**
 *  
 */
function actionMail() {

    $member_files_directories = array(
        'M:/backups/infowap old/memberfiles',
        'M:/backups/files/infowap/public_html/memberfiles'
    );

    $emails = array();

    foreach($member_files_directories as $dir) {

        if ($handle = opendir($dir)) {

            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($handle))) {

                $parts = explode('.', $entry);
                if(end($parts) == 'dat') {

                    $file = file_get_contents($dir.'/'.$entry);
                    $info = explode("|", $file);
                    unset($file);
                    if(!empty($info[2]) && !empty($info[15]))
                        $emails[$info[2]] = $info[15];
                }

            }

            closedir($handle);

        }

    }
    Yii::app()->db->setActive(false);
    Yii::app()->db->connectionString = 'mysql:host=localhost;dbname=statistics';
    Yii::app()->db->setActive(true);
    $users = Yii::app()->db->createCommand('select * from users')->queryAll();
    foreach($users as $user) {
        if(!isset($emails[$user['email']]))
            $emails[$user['email']] = $user['link'];
    }
    echo count($emails);


    Yii::app()->db->setActive(false);
    Yii::app()->db->connectionString = 'mysql:host=localhost;dbname=infowap';
    Yii::app()->db->setActive(true);
    foreach($emails as $email => $site) {

        Yii::app()->db->createCommand()->insert('tbl_emails', array(
            'email'=>$email,
            'site'=>$site,
        ));

    }

    exit;

}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}