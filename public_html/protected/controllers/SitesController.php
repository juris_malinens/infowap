<?php

class SitesController extends Controller
{
    public function actionDelete()
    {
            $this->render('delete');
    }

    public function actionEdit($id)
    {
        $categories = CHtml::listData(Categories::model()->findAll(), 'id', 'name');
        $model = Sites::model()->find('id=:id', array(':id'=>$id));
        if($model->user_id != $user = Yii::app()->getModule('user')->user()->id) {
            echo "This is not Your site. Violation logged";
            exit;
        }
        $this->render('edit', array('model'=>$model, 'categories' => $categories));
    }
    
    public function actionInfo($id)
    {
        //var_dump($_GET);
        //var_dump($id);
        $site = Sites::model()->find('id=:id', array(':id'=>$id));
        $this->render('info', array('site'=>$site));
    }

    public function actionAdd()
    {
        $model=new Sites;
        
        $categories = CHtml::listData(Categories::model()->findAll(), 'id', 'name');
        
        $this->render('add', array('model'=>$model, 'categories' => $categories));
    }

    public function actionIndex()
    {
        
        $user = Yii::app()->getModule('user')->user();
        if(!$user)
            $this->redirect('/');
        
        $sites = Sites::model()->findAll('user_id=:user_id', array(':user_id'=>$user->id));
        
        $this->render('index', array('sites'=>$sites));
    }
        
    public function actionCreate()
    {
        $model=new Sites;
        $this->performAjaxValidation($model);
        if(isset($_POST['Sites']))
        {
            
            $_POST['Sites']['user_id'] = Yii::app()->getModule('user')->user()->id;
            $model->attributes=$_POST['Sites'];
            if($model->save()) {
                //$this->redirect('index');
                //echo "saved OK";
            } else {
                //echo "saved NOT OK";
                //echo 'problem adding wapsite: '.print_r($model->getErrors());
            }
                
        }
        $this->render('create',array('model'=>$model));
    }
    
    public function actionSave()
    {
        
        $model = Sites::model()->find('id=:id', array(':id'=>$_POST['Sites']['id']));
        
        if($model->user_id != $user = Yii::app()->getModule('user')->user()->id) {
            echo "This is not Your site. Violation logged";
            exit;
        }
        $this->performAjaxValidation($model);
        if(isset($_POST['Sites']))
        {
            
            $model->attributes=$_POST['Sites'];
            if($model->update()) {
                
                $this->redirect('index');
                
            }
                
        }
        $this->redirect('index');
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='sites-add-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('info'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','add', 'edit', 'create', 'info', 'save'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'edit', 'save'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}