<?php

require_once dirname(__FILE__) . '/../../../../../controllers/CategoriesController.php';

/**
 * Test class for CategoriesController.
 * Generated by PHPUnit on 2012-10-15 at 22:28:26.
 */
class CategoriesControllerTest extends CTestCase {

    /**
     * @var CategoriesController
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->object = new CategoriesController;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * Generated from @assert ('Test') === 'test'.
     */
    public function testTo_seo() {
        $this->assertSame(
                'test', $this->object->to_seo('Test')
        );
    }

    /**
     * Generated from @assert ('Test#') === 'test'.
     */
    public function testTo_seo2() {
        $this->assertSame(
                'test', $this->object->to_seo('Test#')
        );
    }

    /**
     * @todo Implement testFilters().
     */
    public function testFilters() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testAccessRules().
     */
    public function testAccessRules() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionView().
     */
    public function testActionView() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionCreate().
     */
    public function testActionCreate() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionUpdate().
     */
    public function testActionUpdate() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionDelete().
     */
    public function testActionDelete() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionIndex().
     */
    public function testActionIndex() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionCategory().
     */
    public function testActionCategory() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testActionAdmin().
     */
    public function testActionAdmin() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @todo Implement testLoadModel().
     */
    public function testLoadModel() {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}

?>
