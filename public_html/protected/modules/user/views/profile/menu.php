<ul class="actions">
<?php 
if(UserModule::isAdmin()) {
?>
<li><?php echo CHtml::link(UserModule::t('Manage User'),array('/user/admin')); ?></li>
<?php 
} else {
    
?>
<!--<li><?php //echo CHtml::link(UserModule::t('List User'),array('/user')); ?></li>-->
<?php
}
?>
<li><?php echo CHtml::link(UserModule::t('Add new mobile website'),array('/sites/add')); ?></li>
<li><?php echo CHtml::link(UserModule::t('My sites'),array('/sites')); ?></li>
<li><?php echo CHtml::link(UserModule::t('Profile'),array('/user/profile')); ?></li>
<li><?php echo CHtml::link(UserModule::t('Edit'),array('edit')); ?></li>
<li><?php echo CHtml::link(UserModule::t('Change password'),array('changepassword')); ?></li>
<li><?php echo CHtml::link(UserModule::t('Logout'),array('/user/logout')); ?></li>
</ul>