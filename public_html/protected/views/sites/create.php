<div class="form">
<p>Mobile website added!</p>
<?php
//print_r($model);
?>
<p>Add this link to Toplist:</p>
<p>
    Link:<br/>
    <span style="font-weight: bolder">
        http://infowap.info/clicks/<?=Yii::app()->db->getLastInsertId()?>
    </span>
    Title:<br/>
    <span style="font-weight: bolder">
        Mobile websites @ Infowap
    </span>
    <br/>
    Or HTML code:<br/>
    <span style="font-weight: bolder">
    <?=htmlspecialchars('<a href="http://infowap.info/clicks/'.
            Yii::app()->db->getLastInsertId().'">Mobile websites @ Infowap</a>')?>
    </span>
</p>
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'sites-add-form',
	'enableAjaxValidation'=>FALSE,
        'action' => 'create'
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->