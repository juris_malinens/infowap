<?php
$this->breadcrumbs=array(
	'Sites',
);?>
<h1>My sites</h1>

<ul>
    <li>
<?php echo CHtml::link(UserModule::t('Add mobile website'),array('/sites/add')); ?>
    </li>
</ul>

<?php if($sites): ?>
<ul class="auto_height">
    <?php foreach($sites as $site):
        //print_r($site->user->username);
    ?>
    <li>
<?=CHtml::link($site['title'].' ['.UserModule::t('Edit').']',array('/sites/edit/'.$site['id'])); ?>
    <br/>
    Link:<br/>
    <span style="font-weight: bolder">
        http://infowap.info/clicks/<?=$site['id']?>
    </span>
    <br/>
    Title:<br/>
    <span style="font-weight: bolder">
        Mobile websites @ Infowap
    </span>
    <br/>
    Or HTML code:<br/>
    <span style="font-weight: bolder">
    <?=htmlspecialchars('<a href="http://infowap.info/clicks/'.
            $site['id'].'">Mobile websites @ Infowap</a>')?>
    </span>
    <div class="clearfix"></div>
    </li>
    <?php endforeach; ?>
</ul>
<?php else: ?>
You haven't added any mobile websites yet!<br/>
<?php endif; ?>
<ul>
    <li>
<?php echo CHtml::link(UserModule::t('Add mobile website'),array('/sites/add')); ?>
    </li>
</ul>
