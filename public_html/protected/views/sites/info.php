<?php
$this->breadcrumbs=array(
	$site->category->name=>array('/'.$site->category->link),
	$site['title'],
);
?>
<h1><?php //print_r($site);
echo $this->id . '/' . $this->action->id; ?></h1>

<ul>
    <li class="first">
        <h3><?='Title: '.$site['title']?></h3>
    </li>
    <li class="small">
<?=CHtml::link(Yii::t('infowap', 'WAP site: '.str_replace('http://', '',$site['wap_link'])),array('/clicks/out/wap/'.$site['id']))?>
    </li>
    <?php if($site['web_link']): ?>
    <li class="small">
<?=CHtml::link(Yii::t('infowap', 'Website: '.str_replace('http://', '',$site['web_link'])),array('/clicks/out/web/'.$site['id']))?>
    </li>
    <?php endif; ?>
    <li class="more last">
        <h3>Description:</h3>
        <p><?=htmlspecialchars($site['description'])?></p>
        <div class="clearfix"></div>
    </li>
    
</ul>


<!--<?=$site['description']?>
<?=$site['category_id']?>
<?=$site['user_id']?>-->
