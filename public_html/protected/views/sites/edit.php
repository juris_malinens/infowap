<?php
$this->breadcrumbs=array(
	'Sites'=>array('/sites'),
	'Edit',
);?>
<div class="form">

<?php
//$model=new Sites;
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'sites-add-form',
	'enableAjaxValidation'=>FALSE,
        'action' => '/sites/save'
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php //echo $form->labelEx($model,'user_id'); ?>
                <?php echo $form->hiddenField($model,'id'); ?>
		<?php //echo $form->hiddenField($model,'user_id'); ?>
		<?php //echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wap_link'); ?>
		<?php echo $form->textField($model,'wap_link'); ?>
		<?php echo $form->error($model,'wap_link'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
		<?php echo $form->listBox($model,'category', $categories); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'web_link'); ?>
		<?php echo $form->textField($model,'web_link'); ?>
		<?php echo $form->error($model,'web_link'); ?>
	</div>

	<div class="row">
            <p class="note">
                Describe Your mobile website.<br/>
                Use correct English.<br/>
                If description is inconsistent with site's content, this site will be banned</p>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->