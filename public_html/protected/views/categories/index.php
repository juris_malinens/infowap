<?php
/*$this->breadcrumbs=array(
	'Categories',
);*/

if(Yii::app()->getModule('user')->isAdmin()) {
    $this->menu=array(
            array('label'=>'Create Categories', 'url'=>array('create')),
            array('label'=>'Manage Categories', 'url'=>array('admin')),
    );
}
?>

<!--<p>Active sites: <?=$active_sites?></p>-->
<ul>
    <?php foreach($categories as $nId => $category): ?>
    
    <?php if($category['count'] > 0): ?>
    
    <?php if($nId == 0): ?>
    <li class="first">
    <?php elseif($nId == count($categories)-1): ?>
    <li class="last">
    <?php else: ?>
    <li>
    <?php endif; ?>
    <?=CHtml::link(Yii::t('infowap', $category['name']).' ['.$category['count'].']',
    array('/'.$category['link']))?>
        <span></span>
    </li>
    
    <?php endif; ?>
    
    <?php endforeach; ?>
</ul>