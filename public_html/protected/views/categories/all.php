<h2>Top mobile sites</h2>
<!--<p>Active sites: <?=$active_sites?></p>-->
<?php if($sites): ?>
<ul class="double_height">
    <?php foreach($sites as $nId => $site): ?>
    
    <?php
    $sClass = " ";
    if($nId == 0)
        $sClass .= " first";
    if($nId == count($sites)-1)
        $sClass .= " last";
    ?>
    <li class="clearfix<?=$sClass?>">
    <?=CHtml::link(htmlspecialchars($site['title']),
    array('/sites/info/'.$site['id'].'-'.$site['seo_link']))?>
    <?=CHtml::link(
    str_replace('http://','', $site['wap_link']).' ['.$site['sum_in'].'/'.$site['sum_out'].']',
    array('/sites/info/'.$site['id'].'-'.$site['seo_link']),
    array('class'=>'second_link'))?>
    </li>
    <?php endforeach; ?>
</ul>
<?php else: ?>
No sites active yet. Be first one to add mobile website here!
<?php endif; ?>

<?php
function ob_callback($buffer)
{
  //replace all the apples with oranges
  $buffer = str_replace("categories/category/all/", "", $buffer);
  $buffer = str_replace("/page", '', $buffer);
  return $buffer;
  
}
ob_start('ob_callback');
$this->widget('CLinkPager', array(
    'pages' => $pages,
    'cssFile' => false,
    'firstPageLabel' => '&lt;&lt;',
    'lastPageLabel' => '&gt;&gt;',
    'nextPageLabel' => '&gt;',
    'prevPageLabel' => '&lt;'
));
ob_end_flush();
?>
<div class="clearfix"></div>

