<?php

/**
 * This is the model class for table "{{sites}}".
 *
 * The followings are the available columns in table '{{sites}}':
 * @property string $id
 * @property integer $user_id
 * @property string $title
 * @property string $wap_link
 * @property string $web_link
 * @property integer $category_id
 * @property string $description
 * @property integer $is_banned
 * @property string $ban_reason
 *
 * The followings are the available model relations:
 * @property Clicks[] $clicks
 * @property Categories $category
 * @property Users $user
 * @property Statistics[] $statistics
 */
class Sites extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sites}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, title, wap_link, category_id', 'required'),
			array('user_id, category_id, is_banned', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>25),
			array('wap_link, web_link', 'length', 'max'=>40),
			array('description, ban_reason', 'length', 'max'=>1024),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, title, wap_link, web_link, category_id, description, is_banned, ban_reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clicks' => array(self::HAS_MANY, 'Clicks', 'site_id'),
			'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'statistics' => array(self::HAS_MANY, 'Statistics', 'site_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'title' => 'Title',
			'wap_link' => 'WAP Link',
			'web_link' => 'WEB Link',
			'category_id' => 'Category',
			'description' => 'Description',
			'is_banned' => 'Is Banned',
			'ban_reason' => 'Ban Reason',
		);
	}
        
protected function beforeSave()
{
    if(parent::beforeSave())
    {
        if($this->isNewRecord)
        {
            $this->user_id = Yii::app()->getModule('user')->user()->id;
        }
        return true;
    }
    else
        return false;
}
        
        
        public function getUrl()
        {
            return Yii::app()->createUrl('sites/info', array(
                'id'=>$this->id,
                'title'=>$this->title,
            ));
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('wap_link',$this->wap_link,true);
		$criteria->compare('web_link',$this->web_link,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('is_banned',$this->is_banned);
		$criteria->compare('ban_reason',$this->ban_reason,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}