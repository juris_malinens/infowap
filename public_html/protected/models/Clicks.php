<?php

/**
 * This is the model class for table "{{clicks}}".
 *
 * The followings are the available columns in table '{{clicks}}':
 * @property string $id
 * @property string $site_id
 * @property string $datetime
 * @property string $user_agent
 * @property string $ip
 * @property string $continent
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $organization
 * @property integer $is_opera_mini
 * @property string $referer
 * @property integer $is_click_good
 * @property integer $is_ip_blocked
 *
 * The followings are the available model relations:
 * @property Sites $site
 */
class Clicks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Clicks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{clicks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('site_id', 'required'),
			array('is_opera_mini, is_click_good, is_ip_blocked', 'numerical', 'integerOnly'=>true),
			array('site_id', 'length', 'max'=>10),
			array('user_agent', 'length', 'max'=>256),
			array('ip', 'length', 'max'=>15),
			array('continent, country', 'length', 'max'=>2),
			array('region, city, organization, referer', 'length', 'max'=>45),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, site_id, datetime, user_agent, ip, continent, country, region, city, organization, is_opera_mini, referer, is_click_good, is_ip_blocked', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'site' => array(self::BELONGS_TO, 'Sites', 'site_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'site_id' => 'Site',
			'datetime' => 'Datetime',
			'user_agent' => 'User Agent',
			'ip' => 'Ip',
			'continent' => 'Continent',
			'country' => 'Country',
			'region' => 'Region',
			'city' => 'City',
			'organization' => 'Organization',
			'is_opera_mini' => 'Is Opera Mini',
			'referer' => 'Referer',
			'is_click_good' => 'Is Click Good',
			'is_ip_blocked' => 'Is Ip Blocked',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('site_id',$this->site_id,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('continent',$this->continent,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('organization',$this->organization,true);
		$criteria->compare('is_opera_mini',$this->is_opera_mini);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('is_click_good',$this->is_click_good);
		$criteria->compare('is_ip_blocked',$this->is_ip_blocked);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}