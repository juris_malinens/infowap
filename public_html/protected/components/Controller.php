<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

if (strtolower(substr(PHP_OS, 0, 3)) == 'win') {
    define('WURFL_DIR', 'F:/xampp/htdocs/bwap.localhost/global/classes/ScientiaMobile/WURFL/');
    define('WURFL_CACHE_DIR', 'F:/xampp/htdocs/bwap.localhost/global/cache/WURFL/');
} else {
    define('WURFL_DIR', '/home/bwap/global/classes/ScientiaMobile/WURFL/');
    define('WURFL_CACHE_DIR', '/home/bwap/global/cache/WURFL/');
}
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        
        public $wurflConfig, $wurflManager, $device, $aMobileInfo, $capabilities = array();
        
        
        function beforeAction($action) {
            
            Yii::app()->clientScript->scriptMap=array(
                    'jquery.js'=>false,
            );
            
            //Yii::app()->theme = $this->deviceTheme;
            //Yii::app()->session['wurfl'] = '';
            if(empty(Yii::app()->session['wurfl'])) {
                
                Yii::import('application.vendors.*');
                spl_autoload_unregister(array('YiiBase','autoload'));
                require WURFL_DIR."Application.php";
                //print_r(Yii::app()->session['wurfl']);
            
                $this->wurflConfig = new WURFL_Configuration_InMemoryConfig();
                // Set location of the WURFL File
                $this->wurflConfig->wurflFile(WURFL_DIR.'wurfl-latest.zip');
                // Set the match mode for the API ('performance' or 'accuracy')
                $this->wurflConfig->matchMode('performance');
                // Setup WURFL Persistence
                $this->wurflConfig->persistence('file',
                        array('dir' => WURFL_CACHE_DIR.'persistence'));
                // Setup Caching
                $this->wurflConfig->cache('file', 
                        array('dir' => WURFL_CACHE_DIR.'cache', 'expiration' => WURFL_Storage::ONE_MONTH));

                $wurflManagerFactory = new WURFL_WURFLManagerFactory($this->wurflConfig);

                spl_autoload_register(array('YiiBase','autoload'));

                $this->wurflManager = $wurflManagerFactory->create();
                $this->device = $this->wurflManager->getDeviceForHttpRequest($_SERVER);

                $this->capabilities = $this->device->getAllCapabilities();
                //$sModelName = $this->device->getCapability("model_name");
                //$userAgent = $this->device->userAgent;
                //Yii::app()->session['device'] = $sModelName;
                //Yii::app()->session['device_id'] = $this->device->id;
                $sPrefDTD = $this->device->getCapability("html_preferred_dtd");
                $bIsWireless = ($this->device->getCapability('is_wireless_device') == 'false') ? FALSE : TRUE;
                
                $this->aMobileInfo['is_mobile'] = $bIsWireless;

                if($bIsWireless && $sPrefDTD == 'html5')
                    $this->aMobileInfo['is_smartphone'] = TRUE;
                else
                    $this->aMobileInfo['is_smartphone'] = FALSE;

                Yii::app()->session['wurfl'] = $this->aMobileInfo;
                
                /**
                 * or use detectmobilebrowser extension
                 * if (Yii::app()->detectMobileBrowser->showMobile) {
                 *      // do something, like using a different layout/theme
                 *  }
                 */
                    
            
            }
            $aServerName = explode(".", $_SERVER['SERVER_NAME']);
            if($aServerName[0] == 'm')
                Yii::app()->theme = 'mobile';
            elseif($aServerName[0] == 'www')
                Yii::app()->theme = 'v2';
            elseif(Yii::app()->session['wurfl']['is_mobile'])
                Yii::app()->theme = 'mobile';
            else
                Yii::app()->theme = 'v2';
            
            
            //print_r(Yii::app()->session['wurfl']);
            
            return TRUE;
            
        }
        
}