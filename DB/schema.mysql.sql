SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_sites` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `title` VARCHAR(45) NOT NULL ,
  `wap_link` VARCHAR(65) NOT NULL ,
  `web_Link` VARCHAR(65) NULL DEFAULT NULL ,
  `category` INT(11) NOT NULL DEFAULT 1 ,
  `description` VARCHAR(1024) NULL DEFAULT NULL ,
  INDEX `fk_site_category` (`category` ASC) ,
  PRIMARY KEY (`id`, `user_id`, `category`) ,
  UNIQUE INDEX `link_UNIQUE` (`wap_link` ASC) ,
  INDEX `fk_tbl_sites_tbl_users1` (`user_id` ASC) ,
  UNIQUE INDEX `web_Link_UNIQUE` (`web_Link` ASC) ,
  CONSTRAINT `fk_site_category`
    FOREIGN KEY (`category` )
    REFERENCES `infowap`.`tbl_categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_sites_tbl_users1`
    FOREIGN KEY (`user_id` )
    REFERENCES `infowap`.`tbl_users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_categories` (
  `id` INT(11) NOT NULL ,
  `name` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_statistics` (
  `id` INT(11) NOT NULL ,
  `site_id` INT(10) UNSIGNED NOT NULL ,
  `in` INT(10) UNSIGNED NULL DEFAULT 0 ,
  `out` INT(10) UNSIGNED NULL DEFAULT 0 ,
  `date` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`id`, `site_id`) ,
  INDEX `fk_statistics_site1` (`site_id` ASC) ,
  CONSTRAINT `fk_statistics_site1`
    FOREIGN KEY (`site_id` )
    REFERENCES `infowap`.`tbl_sites` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_clicks` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `site_id` INT(10) UNSIGNED NOT NULL ,
  `datetime` DATETIME NULL DEFAULT NULL ,
  `user_agent` VARCHAR(256) NULL DEFAULT NULL ,
  `ip` VARCHAR(15) NULL DEFAULT NULL ,
  `country` VARCHAR(2) NULL DEFAULT NULL ,
  `city` VARCHAR(45) NULL DEFAULT NULL ,
  `referer` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`, `site_id`) ,
  INDEX `fk_click_site1` (`site_id` ASC) ,
  CONSTRAINT `fk_click_site1`
    FOREIGN KEY (`site_id` )
    REFERENCES `infowap`.`tbl_sites` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(20) NOT NULL ,
  `password` VARCHAR(128) NOT NULL ,
  `email` VARCHAR(128) NOT NULL ,
  `activkey` VARCHAR(128) NOT NULL DEFAULT '' ,
  `createtime` INT(10) NOT NULL DEFAULT '0' ,
  `lastvisit` INT(10) NOT NULL DEFAULT '0' ,
  `superuser` INT(1) NOT NULL DEFAULT '0' ,
  `status` INT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `username` (`username` ASC) ,
  UNIQUE INDEX `email` (`email` ASC) ,
  INDEX `status` (`status` ASC) ,
  INDEX `superuser` (`superuser` ASC) ,
  CONSTRAINT `fk_tbl_users_tbl_profiles1`
    FOREIGN KEY (`id` )
    REFERENCES `infowap`.`tbl_profiles` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_profiles_fields` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `varname` VARCHAR(50) NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `field_type` VARCHAR(50) NOT NULL ,
  `field_size` INT(3) NOT NULL DEFAULT '0' ,
  `field_size_min` INT(3) NOT NULL DEFAULT '0' ,
  `required` INT(1) NOT NULL DEFAULT '0' ,
  `match` VARCHAR(255) NOT NULL DEFAULT '' ,
  `range` VARCHAR(255) NOT NULL DEFAULT '' ,
  `error_message` VARCHAR(255) NOT NULL DEFAULT '' ,
  `other_validator` VARCHAR(5000) NOT NULL DEFAULT '' ,
  `default` VARCHAR(255) NOT NULL DEFAULT '' ,
  `widget` VARCHAR(255) NOT NULL DEFAULT '' ,
  `widgetparams` VARCHAR(5000) NOT NULL DEFAULT '' ,
  `position` INT(3) NOT NULL DEFAULT '0' ,
  `visible` INT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) ,
  INDEX `varname` (`varname` ASC, `widget` ASC, `visible` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE  TABLE IF NOT EXISTS `infowap`.`tbl_profiles` (
  `user_id` INT(11) NOT NULL ,
  `lastname` VARCHAR(50) NOT NULL ,
  `firstname` VARCHAR(50) NOT NULL ,
  `birthday` DATE NOT NULL ,
  PRIMARY KEY (`user_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
