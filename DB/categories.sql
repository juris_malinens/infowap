/*
-- Query: 
-- Date: 2012-05-23 17:32
*/
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (1,'Portals','portals','Mobile portals with diverse information in one place');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (2,'Downloads','downloads','Fill Your mobile phone memory full with wide range of mobile content like apps, ringtones, wallpapers');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (3,'News','news','Read what''s new in politics, sports, culture and entertainment from Your mobile phone');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (4,'Services','useful','Services like weather forecast, public transportation, translation, WAP master tools, constructors and search on Your mobile phone');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (5,'Social','community','List of mobile social networking sites or WAP sites related to big social networks like Facebook, Flickr, Twitter, Google+, Foursquare and Instagram');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (6,'Technology','tech','Technology news, gadget reviews, tech discussions');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (7,'Entertainment','fun','Celebrity news, multimedia content for mobile phones, jokes');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (8,'Sports','sports','Sports related news and activities');
INSERT INTO `tbl_categories` (`id`,`name`,`link`,`description`) VALUES (9,'Other','misc','Special mobile sites with specifi content');
