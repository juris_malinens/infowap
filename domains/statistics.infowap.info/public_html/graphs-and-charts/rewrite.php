<?php
if (!isset($_GET['uid']) || !is_numeric($_GET['uid'])) {
    header("location: http://statistics.infowap.info");
    exit;
}
$uid = intval($_GET['uid']);
header("content-type:text/html; charset=utf-8;");

require_once "../configtop.php";
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>infowap.info free wap statistics system for wapsites</title>
    <style type="text/css"> 
        .header  { background: #e0ffe0; color: green; border-bottom: solid 1px green; margin: 0 0 5px 0; padding: 2px; }
        .footer  { background: #e0ffe0; color: green; border-top: solid 1px green; margin: 10px 0 0 0; }
        .company { font-weight: bold; }
        hr { clear: both; border:solid; border-width:1px; border-bottom-color:#007300; border-top-color:#ffffff; border-left-color:#ffffff; border-right-color:#ffffff;}
        .top-news img { float: left; margin-right: 5px; }
        .top-news h3, .news h3 { font-size: large; font-weight: bold; }
        .accesskey { text-decoration: underline; }
        a { text-decoration: none; }
        .validation { margin-top: 10px; }
        .product img { float: left; margin-right: 5px; }
        .product h3, .news h3 { font-size: large; font-weight: bold; }
    </style>
  </head>
  <body>
    <div class="header">
      Free counter stats<br/>
    </div>
    <div class="product">
<div class="product">
    <br/><strong>Statistics</strong><br/><br/></div>
<br/>
<?php
// ------------------------------ вывод статистики пользователя ---------------------
//проверка  ввода id
// проверка существования учетной записи
$result = mysqli_query($db, "SELECT site_name,link,admin,info FROM users WHERE uid=$uid");
$row = mysqli_fetch_array($result);
if (empty($row))
    exit;

//проверка  ввода action
if (empty($_GET['act']))
    $act = "";
else
    $act = $_GET['act'];

$site_name = htmlspecialchars($row['site_name']);
$link = $row['link'];
$admin = htmlspecialchars($row['admin']);
$info = htmlspecialchars($row['info']);

$badwords = array("viagra", "fuck", "ass", "anal", "mature", "xxx", "young", "lesbia", "gay", "porn", "p0rn", "rape",
    "blow", "pussy", "naked", "sex", "shemale", "boob", "dildo", "nude", "strip", "mastur");
$goowords = array("***", "***", "***", "***", "***", "***", "***", "***", "***", "***", "***", "***",
    "***", "***", "***", "***", "***", "***", "***", "***", "***", "***");

function in_arrayi($needle, $haystack) {
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

$info = str_ireplace($badwords, $goowords, $info);
$site_name = str_ireplace($badwords, $goowords, $site_name);

switch ($act) {

    case "continent":
        echo "<div class=\"header\">Site name: $site_name<br/>";
        echo "Wapsite address: <a href=\"/outtop.php?uid=$uid\">$link</a><br/>";
        echo "Graphic- visits by continents:<br/>\n";
        echo "<img src=\"/graphs-and-charts/wap-continents.php?uid=$uid\" alt=\"continent photo\"/><br/>\n";
        echo "<br/>Statistics by continents:<br/>\n";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        $sqlx = mysqli_query($db, "SELECT * FROM `continent` WHERE `site` = '$uid'");
        $rowx = mysqli_fetch_array($sqlx);
        $kp = $rowx[1] + $rowx[2] + $rowx[3] + $rowx[4] + $rowx[5] + $rowx[6];
        $u1 = round($rowx[1] / $kp * 100);
        $u2 = round($rowx[2] / $kp * 100);
        $u3 = round($rowx[3] / $kp * 100);
        $u4 = round($rowx[4] / $kp * 100);
        $u5 = round($rowx[5] / $kp * 100);
        $u6 = round($rowx[6] / $kp * 100);
        echo "South America: " . $rowx[1] . " ($u1%)<br/>\n";
        echo "North America: " . $rowx[2] . " ($u2%)<br/>\n";
        echo "Europe: " . $rowx[3] . " ($u3%)<br/>\n";
        echo "Asia: " . $rowx[4] . " ($u4%)<br/>\n";
        echo "Africa: " . $rowx[5] . " ($u5%)<br/>\n";
        echo "Oceania: " . $rowx[6] . " ($u6%)<br/><br/>\n";
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "country":
        echo "<div class=\"header\">Site name: $site_name<br/>";
        echo "WAP address: <a href=\"/outtop.php?uid=$uid\">$link</a><br/>";
        echo "Graphic- visits by countries:<br/>\n";
        echo "<img src=\"/graphs-and-charts/wap-country.php?uid=$uid\" alt=\"country photo\"/><br/>\n";
        echo "<br/>Statistics by countries (TOP 25):<br/>\n";
        $sqlx = mysqli_query($db, "SELECT * FROM `country` WHERE `site` = '" . intval($_GET['uid']) . "'");
        $rowx = mysqli_fetch_array($sqlx, MYSQL_ASSOC);
        unset($rowx['site']);
        arsort($rowx);
        $rowx = array_slice($rowx, 0, 25);
        foreach ($rowx as $index => $value) {
            echo $index . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "cookies":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "Cookies enabled",
            "Cookies disabled"
        );

        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_kuuku_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_kuuku_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0);
        } else {
            $tel_data = array($viens, $divi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by cookies:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/cookies-enabled-mobile-phones.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by cookies:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }

        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";

        break;
        $result = mysqli_query($db, "SELECT DISTINCT uid,count,host FROM weeks WHERE host > 0 and date=$today ORDER BY host DESC LIMIT $start,$end");
        $count_users_on_page = mysqli_num_rows($result);
    case "top25-countries":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "Cookies enabled",
            "Cookies disabled"
        );

        $sqlx = mysqli_query($db, "SELECT * FROM `1_valstu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs");
        echo"SELECT * FROM `1_valstu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs  ORDER BY host DESC LIMIT 1,25;";
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];
        rsort($rowx);
        print_r($rowx);
        for ($i = 0; $i < 30; $i++) {
            print $rowx[$i] . "<br/>";
        }


        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_valstu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0);
        } else {
            $tel_data = array($viens, $divi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        // вывод инфы
        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by cookies:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/cookies-enabled-mobile-phones.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by cookies:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }

        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";

        break;

    case "gzip":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "GZIP enabled",
            "GZIP disabled"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_saspieshanas_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_saspieshanas_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0);
        } else {
            $tel_data = array($viens, $divi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by GZIP:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/gzip-compression.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by GZIP:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }

        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";

        break;

    case "keyboard":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "normal",
            "qwerty"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_pogu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_pogu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0);
        } else {
            $tel_data = array($viens, $divi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by keyboard:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }
        echo "<img src=\"/graphs-and-charts/keyboard-layouts.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";

        echo "<br/>Statistics by keyboard:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }

        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";

        break;

    case "screen":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "normal",
            "touchscreen"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_ekraanu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_ekraanu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0);
        } else {
            $tel_data = array($viens, $divi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by screen types:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/screen-types.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by screen types:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "internet":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "csd",
            "hscsd",
            "gprs",
            "edge",
            "umts",
            "cdma"
        );

        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`5`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $pieci = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`6`) FROM `1_interneta_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshi = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri, $pieci, $seshi);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by internet:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/network-communication-types.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by internet:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "design":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "bar",
            "clamshell",
            "slider",
            "non-bar"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_dizainu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_dizainu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_dizainu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_dizainu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by design:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/cellphone-designs.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by design:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "connection":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "irda",
            "bluetooth",
            "wifi",
            "no"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_konekciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_konekciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_konekciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_konekciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by connection:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/phone-connections-to-other-devices.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";

        echo "<br/>Statistics by connection:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "colors":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "monochrome",
            "256",
            "4096",
            "65k",
            "262k",
            "16m",
            "32m",
            "unknown"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`5`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $pieci = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`6`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`7`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $septinji = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`8`) FROM `1_kraasu_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $astonji = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri, $pieci, $seshi, $septinji, $astonji);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by colors:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/Mobile-Phone-Colors.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by colors:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "camera":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "0.1",
            "0.3",
            "0.5",
            "1.0",
            "1.2",
            "1.3",
            "1.9",
            "2.0",
            "3.1",
            "5.0",
            "unknown"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`5`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $pieci = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`6`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`7`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $septinji = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`8`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $astonji = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`9`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $devinji = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`10`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $desmit = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`11`) FROM `1_kameru_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $vienpadsmit = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri, $pieci, $seshi, $septinji, $astonji, $devinji, $desmit, $vienpadsmit);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);


        // вывод инфы
        echo "<div class=\"product\">Site name: $site_name<br/>";

        //echo "Wapsite address: <a href=\"outtop.php?uid=$uid\">$link</a><br/>";
        echo "Graphic- visits by camera:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/mobile-phone-cameras.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by camera:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;

    case "resolution":
        $dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array(
            "352x416",
            "320x240",
            "240x320",
            "208x320",
            "208x208",
            "176x220",
            "176x208",
            "132x176",
            "130x130",
            "128x160",
            "128x144",
            "128x128",
            "128x96",
            "120x160",
            "101x80",
            "96x65",
            "unknown"
        );
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`5`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $pieci = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`6`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`7`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $septinji = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`8`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $astonji = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`9`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $devinji = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`10`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $desmit = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`11`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $vienpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`12`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`13`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $triispadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`14`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetrpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`15`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $piecpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`16`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`17`) FROM `1_rezoluuciju_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $septinjpadsmit = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri, $pieci, $seshi, $septinji, $astonji, $devinji, $desmit, $vienpadsmit, $divpadsmit, $triispadsmit, $chetrpadsmit, $piecpadsmit, $seshpadsmit, $septinjpadsmit);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);

        echo "<div class=\"product\">Site name: $site_name<br/>";

        echo "Graphic- visits by resolutions:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/mobile-phone-resolutions.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by resolutions:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        break;



    case "os":
        /*$dayd = (int) $_GET['day'];
        $weekd = (int) $_GET['week'];
        $monthd = (int) $_GET['month'];
        $yeard = (int) $_GET['year'];
        if (!empty($dayd)) {
            $dkverijs = "AND `day`=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "AND `week`=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "AND `month`=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "AND `year`=$yeard";
        } else {
            $ykverijs = "";
        }
        $tel = array("SymbianOS 6.1",
            "SymbianOS 8.0",
            "SymbianOS 7.0",
            "Symbian Series80",
            "SymbianOS 9.1",
            "SymbianOS 9.2",
            "PalmOS",
            "WinMobile 5.x",
            "WinMobile 6.x",
            "WinMobile 2002",
            "WinMobile 2003",
            "WinMobile 2005",
            "MAC (iPhone)",
            "BlackBerry",
            "Symbian UIQ",
            "Linux mobile",
            "Other");
        $sqlx = mysqli_query($db, "SELECT SUM(`1`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $viens = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`2`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`3`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $triis = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`4`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetri = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`5`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $pieci = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`6`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshi = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`7`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $septinji = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`8`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $astonji = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`9`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $devinji = $rowy[0];

        $sqlx = mysqli_query($db, "SELECT SUM(`10`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowx = mysqli_fetch_array($sqlx);
        $desmit = $rowx[0];

        $sqly = mysqli_query($db, "SELECT SUM(`11`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $vienpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`12`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $divpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`13`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $triispadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`14`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $chetrpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`15`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $piecpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`16`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $seshpadsmit = $rowy[0];

        $sqly = mysqli_query($db, "SELECT SUM(`17`) FROM `1_os_statistika` WHERE `uid`=$uid $dkverijs $wkverijs $mkverijs $ykverijs LIMIT 31;");
        $rowy = mysqli_fetch_array($sqly);
        $septinjpadsmit = $rowy[0];

        if (!empty($tel_data)) {
            $tel_data = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            $tel_data = array($viens, $divi, $triis, $chetri, $pieci, $seshi, $septinji, $astonji, $devinji, $desmit, $vienpadsmit, $divpadsmit, $triispadsmit, $chetrpadsmit, $piecpadsmit, $seshpadsmit, $septinjpadsmit);
        }
        $hour = date("H", $New_Time);
        $result = mysqli_query($db, "SELECT `host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        $page = ceil($top_place / $count_top);


        // вывод инфы
        echo "<div class=\"product\">Site name: $site_name<br/>";

        //echo "Wapsite address: <a href=\"outtop.php?uid=$uid\">$link</a><br/>";
        echo "Graphic- visits by OS:<br/>";
        if (!empty($dayd)) {
            $dkverijs = "&amp;day=$dayd";
        } else {
            $dkverijs = "";
        }
        if (!empty($weekd)) {
            $wkverijs = "&amp;week=$weekd";
        } else {
            $wkverijs = "";
        }
        if (!empty($monthd)) {
            $mkverijs = "&amp;month=$monthd";
        } else {
            $mkverijs = "";
        }
        if (!empty($yeard)) {
            $ykverijs = "&amp;year=$yeard";
        } else {
            $ykverijs = "";
        }

        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && !empty($dayd))
            $nrr = "today stats";
        if (!empty($yeard) && !empty($monthd) && !empty($weekd) && empty($dayd)) {
            $nrr = "week stats";
        }
        if (!empty($yeard) && !empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "month stats";
        }
        if (!empty($yeard) && empty($monthd) && empty($weekd) && empty($dayd)) {
            $nrr = "year stats";
        }
        echo $nrr . "<br/>";

        echo "<img src=\"/graphs-and-charts/operating-systems-graph.php?uid=$uid$dkverijs$wkverijs$mkverijs$ykverijs\" alt=\"browser cookies pie chart\"/><br/>";
        echo "<br/>Statistics by OS:<br/>";
        foreach ($tel_data as $index => $value) {
            echo $tel[$index] . ": $value<br/>\n";
        }
        //echo "<br/><a href=\"xhtml-stats.php?act=world/$uid\">Statistics by странам</a><br/>";
        echo "<a href=\"/main/$uid/today.html\">Full statistics</a><br/>";
        //echo "<a href=\"24.php?uid=$uid\">Список всех посещений сегодня</a><br/>";
        */
        break;

    default: // ======================== Statistics участника ======================
        $TimeZone = "+0";
        $New_Time = time() + ($TimeZone * 60 * 60);
        $week_day = date("w", $New_Time);
        $today = date("Ymd", $New_Time);
        $hour = date("H", $New_Time);
        // за сегодня
        $result = mysqli_query($db, "SELECT `count`,`host` FROM `weeks` WHERE `uid`=$uid AND `date`=$today LIMIT 1;");
        $row = mysqli_fetch_array($result);
        $count_today = $row['count'];
        $host_today = $row['host'];

        //----------------------- Место в топе---------------------------
        $result = mysqli_query($db, "SELECT * FROM `weeks` WHERE `host` >= $host_today AND `date`=$today ORDER BY `host` DESC;");
        $top_place = mysqli_num_rows($result);

        // за 24 часа
        $result = mysqli_query($db, "SELECT sum(`count`),sum(`host`) FROM `count_24` WHERE ((`date`=$today-1 AND `hour`>$hour) OR (`date`=$today AND `hour`<=$hour)) AND `uid`=$uid");
        $row = mysqli_fetch_row($result);
        $count_24 = (int) $row[0];
        $host_24 = (int) $row[1];
        // данные за текущий час
        $result = mysqli_query($db, "SELECT `count`,`host` FROM `count_24` WHERE `hour`=$hour AND `date`=$today AND `uid`=$uid");
        $row = mysqli_fetch_row($result);
        // поверка наличия записи в базе
        if (empty($row)) {
            $hour_count = 0;
            $hour_host = 0;
        } else {
            $hour_count = (int) $row[0];
            $hour_host = (int) $row[1];
        }
        // ---------------------------- посещения за месяц --------------------
        $months = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
        $TimeZone = "+0";
        $New_Time = time() + ($TimeZone * 60 * 60);
        $today = date("Ym", $New_Time);
        $index_month = date("m", $New_Time) - 1;
        // данные за текущий месяц
        $result = mysqli_query($db, "SELECT count,host FROM months WHERE date=$today AND uid=$uid");
        $row = mysqli_fetch_row($result);
        // поверка наличия записи в базе
        if (empty($row[0])) {
            $this_month_count = 0;
            $this_month_host = 0;
        } else {
            $this_month_count = (int) $row[0];
            $this_month_host = (int) $row[1];
        }
        //-------------------------- недельная Statistics----------------
        $TimeZone = "+0";
        $New_Time = time() + ($TimeZone * 60 * 60);
        $week_day = date("w", $New_Time);
        $today = date("Ymd", $New_Time);
        // посещений за неделю
        $result = mysqli_query($db, "SELECT sum(count),sum(host) FROM weeks WHERE uid=$uid");
        $row = mysqli_fetch_row($result);
        $total_week_count = (int) $row[0];
        $total_week_host = (int) $row[1];

        // ------------------------ онлайн------------------------
        // подсчитываем онлайн посетителей
        $result = mysqli_query($db, "SELECT count(ip) FROM online_ip WHERE uid=$uid");
        $row = mysqli_fetch_row($result);
        $online = (int) $row[0];

        // -----------------------Всего---------------------------
        $result = mysqli_query($db, "SELECT sum(count),sum(host) FROM months WHERE uid=$uid");
        $row = mysqli_fetch_row($result);
        $all_hits = (int) $row[0];
        $all_hosts = (int) $row[1];

        // -----------------------Пришло \ Ушло---------------------------
        $counts_in_out = mysqli_query($db, "SELECT `out`,`in` FROM `in_out` WHERE `uid`=$uid LIMIT 1");
        $count_row = mysqli_fetch_array($counts_in_out);
        $in_count = (int) $count_row['in'];
        $out_count = (int) $count_row['out'];
        /*
          -----------------------------------------------------------------
          $top_place - место в топе
          $count_today - хитов сегодня
          $host_today - хостов сегодня
          $count_24 - хитов за 24 часа
          $host_24 - хостов за 24 часа
          $hour_count - хитов за этот час
          $hour_host - хостов за этот час
          $this_month_count - хитов за этот месяц
          $this_month_host - хостов за этот месяц
          $total_week_count - хитов за эту неделю
          $total_week_host - хостов за эту неделю
          $online - онлайн посетителей
          $all_hits - всего хитов
          $all_hosts - всего хостов
          $in_count - пришло в топ
          $out_count - ушло из топа
          -----------------------------------------------------------------
         */
        $page = ceil($top_place / $count_top);


        // вывод инфы
        echo "<div class=\"product\">Rank: $top_place<br/>";
        echo "<a href=\"http://ads.bwap.org\">click here for free mobile downloads!</a><br/>";
        echo "Site name: $site_name<br/>";
        echo "Site address: <a href=\"/outtop.php?uid=$uid\">$link</a><br/>";
        echo "Description: $info<br/>";
        echo "Online users: <a href=\"/online_tel.php?uid=$uid\">$online</a><br/>";
        //Statistics телефонов: <a href=\"xhtml-stats.php?act=telinfo/$uid\">Смотреть</a>
        echo "<hr class=\"separator\" />";
        /*
          cookies
          gzip
          keyboard
          screen
          internet
          design
          connection
          colors
          camera
          resolution
          os
          /main/476&act=resolution&year-2008&month-7&week-27&day-4
         */
        $dayxx = date("d");
        $weekxx = date("W");
        $monthxx = date("n");
        $yearxx = date("Y");
#if (empty($stats)) {
        if ($stats == "today") {
            echo"today | <a href=\"/main/$uid/week.html\">week</a> | <a href=\"/main/$uid/month.html\">month</a> | <a href=\"/main/$uid/year.html\">year</a>
<br/>
<a href=\"/keyboard/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
phone keyboard</a><br/>
<a href=\"/screen/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
screen</a><br/>
<a href=\"/internet/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
internet</a><br/>
<a href=\"/design/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
design</a><br/>
<a href=\"/connection/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
connection</a><br/>
<a href=\"/colors/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
colors</a><br/>
<a href=\"/camera/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
camera</a><br/>
<a href=\"/resolution/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
screen resolution</a><br/>
<a href=\"/os/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
operating system</a><br/>
<a href=\"/cookies/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
Cookies</a><br/>
<a href=\"/gzip/$uid/day-$dayxx-week-$weekxx-month-$monthxx-year-$yearxx\">
gzip</a><br/>";
        } elseif ($stats == "week") {
            echo"<a href=\"/main/$uid/today.html\">today</a> | week | <a href=\"/main/$uid/month.html\">month</a> | <a href=\"/main/$uid/year.html\">year</a>
<br/>
<a href=\"/keyboard/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
phone keyboard</a><br/>
<a href=\"/screen/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
screen</a><br/>
<a href=\"/internet/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
internet</a><br/>
<a href=\"/design/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
design</a><br/>
<a href=\"/connection/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
connection</a><br/>
<a href=\"/colors/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
colors</a><br/>
<a href=\"/camera/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
camera</a><br/>
<a href=\"/resolution/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
screen resolution</a><br/>
<a href=\"/os/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
operating system</a><br/>
<a href=\"/cookies/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
Cookies</a><br/>
<a href=\"/gzip/$uid/day--week-$weekxx-month-$monthxx-year-$yearxx\">
gzip</a><br/>";
        } elseif ($stats == "month") {
            echo"<a href=\"/main/$uid/today.html\">today</a> | <a href=\"/main/$uid/week.html\">week</a> | month | <a href=\"/main/$uid/year.html\">year</a>
<br/>
<a href=\"/keyboard/$uid/day--week--month-$monthxx-year-$yearxx\">
phone keyboard</a><br/>
<a href=\"/screen/$uid/day--week--month-$monthxx-year-$yearxx\">
screen</a><br/>
<a href=\"/internet/$uid/day--week--month-$monthxx-year-$yearxx\">
internet</a><br/>
<a href=\"/design/$uid/day--week--month-$monthxx-year-$yearxx\">
design</a><br/>
<a href=\"/connection/$uid/day--week--month-$monthxx-year-$yearxx\">
connection</a><br/>
<a href=\"/colors/$uid/day--week--month-$monthxx-year-$yearxx\">
colors</a><br/>
<a href=\"/camera/$uid/day--week--month-$monthxx-year-$yearxx\">
camera</a><br/>
<a href=\"/resolution/$uid/day--week--month-$monthxx-year-$yearxx\">
screen resolution</a><br/>
<a href=\"/os/$uid/day--week--month-$monthxx-year-$yearxx\">
operating system</a><br/>
<a href=\"/cookies/$uid/day--week--month-$monthxx-year-$yearxx\">
Cookies</a><br/>
<a href=\"/gzip/$uid/day--week--month-$monthxx-year-$yearxx\">
gzip</a><br/>";
        } else {
            echo"<a href=\"/main/$uid/today.html\">today</a> | <a href=\"/main/$uid/week.html\">week</a> | <a href=\"/main/$uid/month.html\">month</a> | year
<br/>
<a href=\"/keyboard/$uid/day--week--month--year-$yearxx\">
phone keyboard</a><br/>
<a href=\"/screen/$uid/day--week--month--year-$yearxx\">
screen</a><br/>
<a href=\"/internet/$uid/day--week--month--year-$yearxx\">
internet</a><br/>
<a href=\"/design/$uid/day--week--month--year-$yearxx\">
design</a><br/>
<a href=\"/connection/$uid/day--week--month--year-$yearxx\">
connection</a><br/>
<a href=\"/colors/$uid/day--week--month--year-$yearxx\">
colors</a><br/>
<a href=\"/camera/$uid/day--week--month--year-$yearxx\">
camera</a><br/>
<a href=\"/resolution/$uid/day--week--month--year-$yearxx\">
screen resolution</a><br/>
<a href=\"/os/$uid/day--week--month--year-$yearxx\">
operating system</a><br/>
<a href=\"/cookies/$uid/day--week--month--year-$yearxx\">
Cookies</a><br/>
<a href=\"/gzip/$uid/day--week--month--year-$yearxx\">
gzip</a><br/>";
        }

        echo "<br/>Today: ";
        echo "hits: $count_today ";
        echo "visitors: $host_today <hr class=\"separator\" />";
        echo "In 24 hours: ";
        echo "hits: $count_24 ";
        echo "visitors: $host_24<hr class=\"separator\" />";
        echo "In this hour: ";
        echo "hits: $hour_count ";
        echo "visitors: $hour_host<hr class=\"separator\" />";
        echo "For this week: ";
        echo "hits: $total_week_count ";
        echo "visitors: $total_week_host<hr class=\"separator\" />";
        echo "For " . $months[$index_month] . ": ";
        echo "hits: $all_hits ";
        echo "visitors: $all_hosts<hr class=\"separator\" />";
        echo "all: ";
        echo "hits: $all_hits ";
        echo "visitors: $all_hosts<hr class=\"separator\" />";
        echo "In/Out stats: ";
        echo "from infowap: $out_count ";
        echo "from site: $in_count<hr class=\"separator\" /></div>";


        // навигация
        echo"<div class=\"header\">";
}
// общая навигация
?>

<a href="/admin.php?uid=$uid">Admin</a><br/>
<a href="/registration.php">Registration</a><br/>
<a href="/index.php">View statistics.infowap.info</a><br/>

<a href="/?p=<?=$page?>">View from site's position</a><br/></div>

<div class="validation"><a href="http://bwap.org">main page</a><br/>
    <a href="/stat.php?uid=<?=$uid?>">WML</a>

    <br/><a href="/?uid=147">
        <img src="/c.php?uid=147" alt="INfoWAP.INfo"/></a><br/>

</div>
</div></body>
</html>
