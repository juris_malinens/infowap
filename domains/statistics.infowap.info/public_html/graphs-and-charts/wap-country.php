<?php
include "../configtop.php";
include "../src/jpgraph.php";
include "../src/jpgraph_pie.php";
include "../src/jpgraph_pie3d.php";
if (is_numeric($_GET['uid'])) {
//viss OK
} else {
    die();
}

$sqlx = mysqli_query($db, "SELECT * FROM `country` WHERE `site` = '" . intval($_GET['uid']) . "'");

$rowx = mysqli_fetch_array($sqlx, MYSQL_ASSOC);
unset($rowx['site']);
arsort($rowx);
$rowx = array_slice($rowx, 0, 6);
$sqly = mysqli_query($db, "SELECT * FROM `country` WHERE `site` = '" . intval($_GET['uid']) . "'");
$t1 = current($rowx);
$u1 = key($rowx);
$t2 = next($rowx);
$u2 = key($rowx);
$t3 = next($rowx);
$u3 = key($rowx);
$t4 = next($rowx);
$u4 = key($rowx);
$t5 = next($rowx);
$u5 = key($rowx);
$t6 = next($rowx);
$u6 = key($rowx);

// Setup the graph

$data = array($t1, $t2, $t3, $t4, $t5, $t6);
$graph = new PieGraph(330, 240, "auto");
$graph->SetShadow();
$graph->title->Set("Country");
$p1 = new PiePlot3D($data);
$p1->SetValueType(PIE_VALUE_PERCENTAGE);
$p1->SetTheme('sand');
$p1->SetColor("black");
$p1->ExplodeSlice(2);
$p1->SetCenter(0.4, 0.6);
$p1->SetStartAngle(90);
$lbl = array("", "", "", "", "", "");
$p1->SetLabels($lbl);
$p1->ExplodeAll(10);

$p1->SetLegends(array("$u1 %.1f%%", "$u2 %.1f%%", "$u3 %.1f%%", "$u4 %.1f%%", "$u5 %.1f%%", "$u6 %.1f%%"));
$graph->Add($p1);
$graph->Stroke();



