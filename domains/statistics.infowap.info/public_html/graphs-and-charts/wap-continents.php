<?php
include "../configtop.php";
include "../src/jpgraph.php";
include "../src/jpgraph_pie.php";
include "../src/jpgraph_pie3d.php";
if (is_numeric($_GET['uid'])) {
//viss OK
} else {
    die();
}

$sqlx = mysqli_query($db, "SELECT * FROM `continent` WHERE `site` = '" . intval($_GET['uid']) . "'");

$rowx = mysqli_fetch_array($sqlx);
$t1 = $rowx[1];
$t2 = $rowx[2];
$t3 = $rowx[3];
$t4 = $rowx[4];
$t5 = $rowx[5];
$t6 = $rowx[6];

// Setup the graph

$data = array($t1, $t2, $t3, $t4, $t5, $t6);
$graph = new PieGraph(330, 240, "auto");
$graph->SetShadow();
$graph->title->Set("Continent");
$p1 = new PiePlot3D($data);
$p1->SetValueType(PIE_VALUE_PERCENTAGE);
$p1->SetTheme('sand');
$p1->SetColor("black");
$p1->ExplodeSlice(2);
$p1->SetCenter(0.4, 0.6);
$p1->SetStartAngle(90);
$lbl = array("", "", "", "", "", "");
$p1->SetLabels($lbl);
$p1->ExplodeAll(10);

$p1->SetLegends(array("SA %.1f%%", "NA %.1f%%", "EU %.1f%%", "AS %.1f%%", "AF %.1f%%", "OC %.1f%%"));
$graph->Add($p1);
$graph->Stroke();
?>


