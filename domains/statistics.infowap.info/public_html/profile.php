<?php
include 'configtop.php';
include 'functions.php';
include 'header.php';

if(!empty($_GET))
    foreach($_GET as $key => $val)
        $_GET[$key] = mysqli_real_escape_string ($db, $_GET[$key]);

if(!empty($_POST))
    foreach($_POST as $key => $val)
        $_POST[$key] = mysqli_real_escape_string ($db, $_POST[$key]);

extract($_REQUEST);

echo " <b><font color=\"#FFFF00\"> Settings</font></b><br><br>";

function verify_addr($address) {
    $return = false;
    if (preg_match('/^[\w.-]+@([\w.-]+)\.[a-z]{2,6}$/i', $address, $domain)) {
        $domain = explode('.', $domain[0]);
        // Split the domain into sections wherein the last element is either 'co', 'org', or the likes, or the primary domain name
        foreach ($domain as $part) { // Iterate through the parts
            if (substr($part, 0, 1) == '_' || substr($part, strlen($part) - 1, 1) == '_')
                $return = false; // The first or last character is _
            else
                $return = true; // The parts are fine. The address seems syntactically valid
        }
    }
    return $return;
}

if (!empty($_POST['uid']) && !empty($_POST['p'])) {
    $uid = (int) $_POST['uid'];
    $pass = $_POST['p'];
    $action = "write";
} else {
    $uid = (int) $_GET['uid'];
    $pass = $_GET['p'];
    $action = "view";
}


if ($uid <= 0) {

    echo "<center><b>Error</b><br/>Wrong id or password";
    echo"</center>";
    echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
    echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
    exit;
}

$result = mysqli_query($db, "SELECT pass FROM users WHERE uid=$uid");
$row = mysqli_fetch_row($result);
if (empty($row[0])) {
    echo "<center><b>error</b><br/>Wrong id or password";
    echo"</center>";
    echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
    echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
    exit;
}
$tpass = $row[0];
if ($tpass != $pass && $tpass != md5($pass)) {
    echo "<center><b>error</b><br/>Wrong id or password";
    echo"</center>";
    echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
    echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
    exit;
}
switch ($action) {
    case "write":
        if (!empty($_POST['site']) && !empty($_POST['link']) && !empty($_POST['admin']) && !empty($_POST['email']) && !empty($_POST['info'])) {
            // изменение данных
            $siteName = mysqli_real_escape_string($db, utf_to_win($_POST['site']));
            $link = mysqli_real_escape_string($db, $_POST['link']);
            $admin = mysqli_real_escape_string($db, utf_to_win($_POST['admin']));
            $email = mysqli_real_escape_string($db, $_POST['email']);
            $info = mysqli_real_escape_string($db, utf_to_win($_POST['info']));

            // проверка Бана пользователя
            $ban_link = parse_url($link);
            $ban_link = $ban_link['host'];
            $result = mysqli_query($db, "SELECT `uid` FROM `ban_user` WHERE `link`='$ban_link'");
            $row = mysqli_fetch_row($result);
            if (!empty($row)) {
                echo "<b>error</b><br/>site blocked! <br>";
                echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
                echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
                exit;
            }
            // проверка существования учетной записи
            $result = mysqli_query($db, "SELECT `uid` FROM `users` WHERE `link`='$link'");
            $row = mysqli_fetch_row($result);
            if (!empty($row)) {
                if ($row[0] != $uid) {
                    echo "<b>error</b><br>site alreadyr registered!";
                    echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
                    echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
                    exit;
                }
            }
            // проверка мыла
            if (!verify_addr($email)) {
                echo "<center><b>OШИБКА</b><br/>Неверный E-Mail!";
                echo"</center>";
                echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
                echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
                exit;
            }

            if (!empty($_POST['npass'])) {
                $npass = $_POST['npass'];
                $pass = md5($npass);
                mysqli_query($db, "UPDATE `users` SET `site_name`='$siteName',`link`='$link',`admin`='$admin',`email`='$email',`info`='$info',`pass`='$pass'  WHERE `uid`=$uid LIMIT 1");
            } else {
                mysqli_query($db, "UPDATE `users` SET `site_name`='$siteName',`link`='$link',`admin`='$admin',`email`='$email',`info`='$info' WHERE `uid`=$uid LIMIT 1");
            }
            header("location: admin.php?uid=$uid&p=$pass&s=edit2");


            exit;
        } else {
            // пустое поле - недопустимо!
            echo "<center><b>error</b><br/>All fields except new password must not be empty! <br> ";
            echo"</center>";
            echo "<br><br><a href=\"profile.php?uid=$uid&amp;p=$pass\">return</a><br>";
            echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
            exit;
        }
        break;
    default:
// данные участника
        $user_data = mysqli_query($db, "SELECT site_name,link,info,admin,email FROM users WHERE uid=$uid");
        $user_data_row = mysqli_fetch_array($user_data);
        $site_name = $user_data_row['site_name'];
        $link = $user_data_row['link'];
        $info = $user_data_row['info'];
        $admin = $user_data_row['admin'];
        $email = $user_data_row['email'];

        echo "<form action=\"profile.php\" method=\"post\">";
        echo "Site name:<br>";
        echo "<input type=\"text\" value=\"" . win_to_utf($site_name) . "\" name=\"site\" /><br>";
        echo "Link to main page:<br>";
        echo "<input type=\"text\" value=\"$link\" name=\"link\" /><br>";
        echo "Your name:<br>";
        echo "<input type=\"text\" value=\"" . win_to_utf($admin) . "\" name=\"admin\" /><br>";
        echo "E-Mail:<br>";
        echo "<input type=\"text\" value=\"$email\" name=\"email\" /><br>";
        echo "Description:<br>";


        echo "<textarea  cols='20' rows='3' name='info'>" . win_to_utf($info) . "</textarea><br>";
        echo "New password:<br>";
        echo "<input type=\"text\" name=\"npass\" />";

        echo "<input type=\"hidden\" value=\"$uid\" name=\"uid\" />";
        echo "<input type=\"hidden\" value=\"$pass\" name=\"p\" /><br><br>";

        echo "<input type=\"submit\" value=\"Change\" />";
        echo "</form><hr>";
        echo "<a href=\"admin.php?uid=$uid&amp;p=$pass\">Admin panel</a><br>";
        echo "<a href=\"index.php\">statistics.infowap.info</a></html>";
        
}
