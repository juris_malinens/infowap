<?php
function utf_to_win($str) {
    $str = strtr($str, array("Р°" => "а", "Р±" => "б", "РІ" => "в", "Рі" => "г", "Рґ" => "д", "Рµ" => "е", "С‘" => "ё", "Р¶" => "ж", "Р·" => "з", "Рё" => "и", "Р№" => "й", "Рє" => "к", "Р»" => "л", "Рј" => "м", "РЅ" => "н", "Рѕ" => "о", "Рї" => "п", "СЂ" => "р", "СЃ" => "с", "С‚" => "т", "Сѓ" => "у", "С„" => "ф", "С…" => "х", "С†" => "ц", "С‡" => "ч", "С€" => "ш", "С‰" => "щ", "СЉ" => "ъ", "С‹" => "ы", "СЊ" => "ь", "СЌ" => "э", "СЋ" => "ю", "СЏ" => "я",
        "Рђ" => "А", "Р‘" => "Б", "Р’" => "В", "Р“" => "Г", "Р”" => "Д", "Р•" => "Е", "РЃ" => "Ё", "Р–" => "Ж", "Р—" => "З", "Р�" => "И", "Р™" => "Й", "Рљ" => "К", "Р›" => "Л", "Рњ" => "М", "Рќ" => "Н", "Рћ" => "О", "Рџ" => "П", "Р " => "Р", "РЎ" => "С", "Рў" => "Т", "РЈ" => "У", "Р¤" => "Ф", "РҐ" => "Х", "Р¦" => "Ц", "Р§" => "Ч", "РЁ" => "Ш", "Р©" => "Щ", "РЄ" => "Ъ", "Р«" => "Ы", "Р¬" => "Ь", "Р­" => "Э", "Р®" => "Ю", "РЇ" => "Я"));
    return $str;
}

function win_to_utf($str) {
    $str = strtr($str, array("а" => "Р°", "б" => "Р±", "в" => "РІ", "г" => "Рі", "д" => "Рґ", "е" => "Рµ", "ё" => "С‘", "ж" => "Р¶", "з" => "Р·", "и" => "Рё", "й" => "Р№", "к" => "Рє", "л" => "Р»", "м" => "Рј", "н" => "РЅ", "о" => "Рѕ", "п" => "Рї", "р" => "СЂ", "с" => "СЃ", "т" => "С‚", "у" => "Сѓ", "ф" => "С„", "х" => "С…", "ц" => "С†", "ч" => "С‡", "ш" => "С€", "щ" => "С‰", "ъ" => "СЉ", "ы" => "С‹", "ь" => "СЊ", "э" => "СЌ", "ю" => "СЋ", "я" => "СЏ",
        "А" => "Рђ", "Б" => "Р‘", "В" => "Р’", "Г" => "Р“", "Д" => "Р”", "Е" => "Р•", "Ё" => "РЃ", "Ж" => "Р–", "З" => "Р—", "И" => "Р�", "Й" => "Р™", "К" => "Рљ", "Л" => "Р›", "М" => "Рњ", "Н" => "Рќ", "О" => "Рћ", "П" => "Рџ", "Р" => "Р ", "С" => "РЎ", "Т" => "Рў", "У" => "РЈ", "Ф" => "Р¤", "Х" => "РҐ", "Ц" => "Р¦", "Ч" => "Р§", "Ш" => "РЁ", "Щ" => "Р©", "Ъ" => "РЄ", "Ы" => "Р«", "Ь" => "Р¬", "Э" => "Р­", "Ю" => "Р®", "Я" => "РЇ"));
    return $str;
}

function rus_utf_tolower($str) {
    $str = strtr($str, array("Рђ" => "Р°", "Р‘" => "Р±", "Р’" => "РІ", "Р“" => "Рі", "Р”" => "Рґ", "Р•" => "Рµ", "РЃ" => "С‘", "Р–" => "Р¶", "Р—" => "Р·", "Р�" => "Рё", "Р™" => "Р№", "Рљ" => "Рє", "Р›" => "Р»", "Рњ" => "Рј", "Рќ" => "РЅ", "Рћ" => "Рѕ", "Рџ" => "Рї", "Р " => "СЂ", "РЎ" => "СЃ", "Рў" => "С‚", "РЈ" => "Сѓ", "Р¤" => "С„", "РҐ" => "С…", "Р¦" => "С†", "Р§" => "С‡", "РЁ" => "С€", "Р©" => "С‰", "РЄ" => "СЉ", "Р«" => "С‹", "Р¬" => "СЊ", "Р­" => "СЌ", "Р®" => "СЋ", "РЇ" => "СЏ",
        "A" => "a", "B" => "b", "C" => "c", "D" => "d", "E" => "e", "I" => "i", "F" => "f", "G" => "g", "H" => "h", "J" => "j", "K" => "k", "L" => "l", "M" => "m", "N" => "n", "O" => "o", "P" => "p", "Q" => "q", "R" => "r", "S" => "s", "T" => "t", "U" => "u", "V" => "v", "W" => "w", "X" => "x", "Y" => "y", "Z" => "z"));
    return $str;
}

if (getenv("HTTP_CLIENT_IP"))
    $ip = getenv("HTTP_CLIENT_IP");
else if (getenv("REMOTE_ADDR"))
    $ip = getenv("REMOTE_ADDR");
else if (getenv("HTTP_X_FORWARDED_FOR"))
    $ip = getenv("HTTP_X_FORWARDED_FOR");
else {
    $ip = "not detected";
}

$currHour = date("H", time());
$currHour = round($currHour);
$currDate = date("d F Y", time());
$curr = date("i:s", time());
$currTime = date("$currHour:i:s", time());
$currTime2 = date("$currHour:i", time());

$currDate = str_replace("January", "Января", $currDate);
$currDate = str_replace("February", "Февраля", $currDate);
$currDate = str_replace("March", "Марта", $currDate);
$currDate = str_replace("April", "Апреля", $currDate);
$currDate = str_replace("May", "Мая", $currDate);
$currDate = str_replace("June", "Июня", $currDate);
$currDate = str_replace("July", "Июля", $currDate);
$currDate = str_replace("August", "Августа", $currDate);
$currDate = str_replace("September", "Сентября", $currDate);
$currDate = str_replace("October", "Октября", $currDate);
$currDate = str_replace("November", "Ноября", $currDate);
$currDate = str_replace("December", "Декабря", $currDate);
$currDate = win_to_utf($currDate);

function check($message) {
    $message = str_replace("|", "I", $message);
    $message = str_replace("||", "I", $message);
    $message = htmlspecialchars($message);
    $message = str_replace("'", "&#39;", $message);
    $message = str_replace("\"", "&#34;", $message);
    $message = str_replace("/\\\$/", "&#036;", $message);
    $message = str_replace("$", "&#036;", $message);
    $message = str_replace("\\", "&#092;", $message);
    $message = str_replace("@", "&#064;", $message);
    $message = stripslashes(trim($message));
    return $message;
}

$browsus = htmlspecialchars(stripslashes(getenv('HTTP_USER_AGENT')));
$brow = strtok($browsus, '(');
$brow = strtok($browsus, ' ');
