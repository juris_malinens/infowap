<?php
ini_set("display_errors", "on");
error_reporting(E_ALL);

include "configtop.php";

if($NEW_VERSION === true) {
    
mysqli_query($db, "DROP TABLE `weeks`");

mysqli_query($db, "
CREATE TABLE IF NOT EXISTS `weeks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `uid` int(10) unsigned NOT NULL default '0',
  `next_mon` int(10) unsigned NOT NULL default '0',
  `date` int(10) unsigned NOT NULL default '0',
  `day_week` smallint(5) unsigned NOT NULL default '0',
  `count` int(10) unsigned NOT NULL default '0',
  `host` int(10) unsigned NOT NULL default '0',
  `in` int(10) unsigned NOT NULL default '0',
  `out` int(10) unsigned NOT NULL default '0',
  `Siemens` int(10) unsigned NOT NULL default '0',
  `Nokia` int(10) unsigned NOT NULL default '0',
  `Samsung` int(10) unsigned NOT NULL default '0',
  `Motorola` int(10) unsigned NOT NULL default '0',
  `LG` int(10) unsigned NOT NULL default '0',
  `Sagem` int(10) unsigned NOT NULL default '0',
  `SonyEricsson` int(10) unsigned NOT NULL default '0',
  `Alcatel` int(10) unsigned NOT NULL default '0',
  `Opera` int(10) unsigned NOT NULL default '0',
  `Mozilla` int(10) unsigned NOT NULL default '0',
  `Panasonic` int(10) unsigned NOT NULL default '0',
  `Other` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `uid` (`uid`,`date`,`day_week`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;
");
/*
mysqli_query($db, "
INSERT INTO weeks (uid,next_mon,date,day_week)
SELECT uid,
DATE_FORMAT(DATE_ADD(CURDATE() , INTERVAL( 9 - IF(DAYOFWEEK(CURDATE())=1, 8, DAYOFWEEK(CURDATE()))) DAY),'%Y%m%d') AS next_mon,
DATE_FORMAT(NOW(),'%Y%m%d') AS date,
0 
FROM months
WHERE
 date = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 month), '%Y%m') AND
 count >= 1
GROUP BY uid 
ORDER BY count DESC
");
*/
}
