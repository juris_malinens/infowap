<?php
/**
 * Delete old online data every minute
 * @author Juris Malinens <juris.malinens@inbox.lv>
 * @version 0.1 27.10.2011- initial version
 */
require_once "configtop.php";

/**
 * Delete old online data every minute
 */
$del_time    = date("ymdHi", mktime(date("H"), date("i")-$online_time));
mysqli_query($db, "DELETE FROM `online_ip` WHERE `time` <= $del_time");
