<?php
ini_set("display_errors", "on");
error_reporting(E_ALL);

include "configtop.php";

$m = new Memcached();
$m->addServer('localhost', 11211);
    
mysqli_query($db, "DELETE FROM rating WHERE 1");
//saliekam jaunās stundas rating datus
$sSql = "SELECT SQL_NO_CACHE uid FROM users";
$q = mysqli_query($db, $sSql);

while($row = mysqli_fetch_object($q)) {
    
    echo $row->uid."\n";
    
    $hosts = (int)$m->get($row->uid.'_hour_hosts');
    $hits = (int)$m->get($row->uid.'_hour_hits');
    
    /*if(apc_exists($row->uid.'_hour_hosts'))
        $hosts = (int)apc_fetch($row->uid.'_hour_hosts');
    else {
        
        apc_add($row->uid.'_hour_hosts', 0, 4000);
        $hosts = 0;
        
    }
    
    if(apc_exists($row->uid.'_hour_hits'))
        $hits = (int)apc_fetch($row->uid.'_hour_hits');
    else {
        
        apc_add($row->uid.'_hour_hits', 0, 4000);
        $hits = 0;
        
    }*/
    
    //if(apc_exists($row->uid.'_hour_hosts') && apc_exists($row->uid.'_hour_hits')) {
    if($hosts > 0 && $hits > 0) {
        
        $sSql = "INSERT INTO `rating` (`uid`, `hosts`, `hits`) VALUES ($row->uid, $hosts, $hits)";
        echo $sSql."\n";
        mysqli_query($db, $sSql);
        
    }
    
    //apc_store($row->uid.'_hour_hosts', 0, 4000);
    //apc_store($row->uid.'_hour_hits', 0, 4000);
    
    if($m->set($row->uid.'_hour_hosts', 0, time()+3600+30) !== TRUE)
        echo 'failed to set '.$row->uid.'_hour_hosts';
    else
        echo 'OK: '.$row->uid.'_hour_hosts';
    if($m->set($row->uid.'_hour_hits', 0, time()+3600+30) !== TRUE)
        echo 'failed to set '.$row->uid.'_hour_hits';
    else
        echo 'OK: '.$row->uid.'_hour_hits';
    
}

//atjaunojam count_24
$q = mysqli_query($db, "SELECT SQL_NO_CACHE * FROM rating");

while($row = mysqli_fetch_object($q)) {
    
    $sSql = "
        INSERT INTO count_24 
            (`uid`, `date`, `hour`, `count`, `host`)
        VALUES
            ($row->uid, CURDATE() + 0, HOUR(NOW())-1, $row->hits, $row->hosts);
    ";
    echo $sSql;
    mysqli_query($db, $sSql);
    
}

$dOneDayAgo = date("Ymd", strtotime("-1 day"));
//dzēšam vecos count_24 ierakstus
$sSql = "
DELETE 
FROM
    count_24
WHERE
    `date` < $dOneDayAgo
";
echo $sSql."\n";
$q = mysqli_query($db, $sSql);

//dzēšam vecos count_24 ierakstus
$sSql = "
SELECT
    count_24.`uid` `uid`,
    COUNT(count_24.`uid`) AS skaits
FROM
    count_24
GROUP BY count_24.`uid` 
HAVING skaits >= 24
ORDER BY COUNT(count_24.uid) DESC
";
echo $sSql."\n";
$q = mysqli_query($db, $sSql);

while($r = mysqli_fetch_array($q)) {

	$diff = $r["skaits"] - 23;

	$s = "
	DELETE 
	FROM
		count_24
	WHERE
		uid = {$r["uid"]}
	ORDER by id ASC 
	LIMIT {$diff}
	";
	//$q = mysqli_query($db, $s);
	
	/*
	pieliekam jaunu stundu kl�t
	*/
	//mysqli_query($db, "INSERT INTO count_24 VALUES ('', {$r["uid"]}, CURDATE() + 0, HOUR(NOW()), 0, 0);");
}



//daily cron
if(date("H") == '00') {
    
mysqli_query($db, "TRUNCATE `hits_ipx3`");
mysqli_query($db, "UPDATE `continent` SET `AS` = '0', `SA` = '0', `AF` = '0', `NA` = '0', `EU` = '0', `OC` = '0' WHERE 1");
mysqli_query($db, "UPDATE `in_out` SET `in` = 0, `out` = 0 WHERE 1");
mysqli_query($db, "UPDATE `country` SET 
	  `AD` = '0', `AE` = '0', `AF` = '0', `AG` = '0',
	  `AI` = '0',  `AL` = '0',  `AM` = '0',  `AN` = '0',  `AO` = '0',  `AP` = '0',  `AQ` = '0',  `AR` = '0',
	  `AS` = '0',  `AT` = '0',  `AU` = '0',  `AW` = '0',  `AZ` = '0',  `BA` = '0',  `BB` = '0',  `BD` = '0',
	  `BE` = '0',  `BF` = '0',  `BG` = '0',  `BH` = '0',  `BI` = '0',  `BJ` = '0',  `BM` = '0',  `BN` = '0',
	  `BO` = '0',  `BR` = '0',  `BS` = '0',  `BT` = '0',  `BV` = '0',  `BW` = '0',  `BY` = '0',  `BZ` = '0',
	  `CA` = '0',  `CC` = '0',  `CD` = '0',  `CF` = '0',  `CG` = '0',  `CH` = '0',  `CI` = '0',  `CK` = '0',
	  `CL` = '0',  `CM` = '0',  `CN` = '0',  `CO` = '0',  `CR` = '0',  `CU` = '0',  `CV` = '0',  `CX` = '0',
	  `CY` = '0',  `CZ` = '0',  `DE` = '0',  `DJ` = '0',  `DK` = '0',  `DM` = '0',  `DO` = '0',  `DZ` = '0',
	  `EC` = '0',  `EE` = '0',  `EG` = '0',  `EH` = '0',  `ER` = '0',  `ES` = '0',  `ET` = '0',  `EU` = '0',
	  `FI` = '0',  `FJ` = '0',  `FK` = '0',  `FM` = '0',  `FO` = '0',  `FR` = '0',  `FX` = '0',  `GA` = '0',
	  `GB` = '0',  `GD` = '0',  `GE` = '0',  `GF` = '0',  `GH` = '0',  `GI` = '0',  `GL` = '0',  `GM` = '0',
	  `GN` = '0',  `GP` = '0',  `GQ` = '0',  `GR` = '0',  `GS` = '0',  `GT` = '0',  `GU` = '0',  `GW` = '0',
	  `GY` = '0',  `HK` = '0',  `HM` = '0',  `HN` = '0',  `HR` = '0',  `HT` = '0',  `HU` = '0',  `ID` = '0',
	  `IE` = '0',  `IL` = '0',  `IN` = '0',  `IO` = '0',  `IQ` = '0',  `IR` = '0',  `IS` = '0',  `IT` = '0',
	  `JM` = '0',  `JO` = '0',  `JP` = '0',  `KE` = '0',  `KG` = '0',  `KH` = '0',  `KI` = '0',  `KM` = '0',
	  `KN` = '0',  `KP` = '0',  `KR` = '0',  `KW` = '0',  `KY` = '0',  `KZ` = '0',  `LA` = '0',  `LB` = '0',
	  `LC` = '0',  `LI` = '0',  `LK` = '0',  `LR` = '0',  `LS` = '0',  `LT` = '0',  `LU` = '0',  `LV` = '0',
	  `LY` = '0',  `MA` = '0',  `MC` = '0',  `MD` = '0',  `MG` = '0',  `MH` = '0',  `MK` = '0',  `ML` = '0',
	  `MM` = '0',  `MN` = '0',  `MO` = '0',  `MP` = '0',  `MQ` = '0',  `MR` = '0',  `MS` = '0',  `MT` = '0',
	  `MU` = '0',  `MV` = '0',  `MW` = '0',  `MX` = '0',  `MY` = '0',  `MZ` = '0',  `NA` = '0',  `NC` = '0',
	  `NE` = '0',  `NF` = '0',  `NG` = '0',  `NI` = '0',  `NL` = '0',  `NO` = '0',  `NP` = '0',  `NR` = '0',
	  `NU` = '0',  `NZ` = '0',  `OM` = '0',  `PA` = '0',  `PE` = '0',  `PF` = '0',  `PG` = '0',  `PH` = '0',
	  `PK` = '0',  `PL` = '0',  `PM` = '0',  `PN` = '0',  `PR` = '0',  `PS` = '0',  `PT` = '0',  `PW` = '0',
	  `PY` = '0',  `QA` = '0',  `RE` = '0',  `RO` = '0',  `RU` = '0',  `RW` = '0',  `SA` = '0',  `SB` = '0',
	  `SC` = '0',  `SD` = '0',  `SE` = '0',  `SG` = '0',  `SH` = '0',  `SI` = '0',  `SJ` = '0',  `SK` = '0',
	  `SL` = '0',  `SM` = '0',  `SN` = '0',  `SO` = '0',  `SR` = '0',  `ST` = '0',  `SV` = '0',  `SY` = '0',
	  `SZ` = '0',  `TC` = '0',  `TD` = '0',  `TF` = '0',  `TG` = '0',  `TH` = '0',  `TJ` = '0',  `TK` = '0',
	  `TM` = '0',  `TN` = '0',  `TO` = '0',  `TP` = '0',  `TR` = '0',  `TT` = '0',  `TV` = '0',  `TW` = '0',
	  `TZ` = '0',  `UA` = '0',  `UG` = '0',  `UM` = '0',  `US` = '0',  `UY` = '0',  `UZ` = '0',  `VA` = '0',
	  `VC` = '0',  `VE` = '0',  `VG` = '0',  `VI` = '0',  `VN` = '0',  `VU` = '0',  `WF` = '0',  `WS` = '0',
	  `YE` = '0',  `YT` = '0',  `YU` = '0',  `ZA` = '0',  `ZM` = '0',  `ZR` = '0',  `ZW` = '0'
	   WHERE 1
");

    $sSql = "SELECT SQL_NO_CACHE uid FROM users";
    echo $sSql."\n";
    $q = mysqli_query($db, $sSql);

    while($row = mysqli_fetch_object($q)) {
        
        echo $row->uid."\n";
        /*if(!apc_exists($row->uid.'_today_hits'))
            apc_add($row->uid.'_today_hits', 0, 90000);
        else
            apc_store($row->uid.'_today_hits', 0, 90000);
        
        if(!apc_exists($row->uid.'_today_hosts'))
            apc_add($row->uid.'_today_hosts', 0, 90000);
        else
            apc_store($row->uid.'_today_hosts', 0, 90000);
        */
        if($m->set($row->uid.'_today_hits', 0, time()+86400+30) !== TRUE)
            echo "\n failed to set: {$row->uid}_today_hits  \n";
        else
            echo "\n OK to set: {$row->uid}_today_hits  \n";
                
        if($m->set($row->uid.'_today_hosts', 0, time()+86400+30) !== TRUE)
            echo "\n failed to set: {$row->uid}_today_hosts  \n";
        else
            echo "\n OK to set: {$row->uid}_today_hosts  \n";
            
    }
}


/*
$q = mysqli_query($db, "
SELECT count_24.uid uid, count(count_24.uid) skaits
FROM count_24
GROUP BY count_24.uid 
ORDER BY count(count_24.uid) DESC
");

while($r = mysqli_fetch_array($q)) {
	
	//pieliekam jauno stundu kl�t
	//mysqli_query($db, "INSERT INTO count_24 VALUES ('', {$r["uid"]}, CURDATE() + 0, HOUR(NOW()), 0, 0);");
}
*/
