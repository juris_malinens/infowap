<?php
error_reporting(0);
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
@include "./configtop.php";
$uid=$_GET['uid'];
$week_day = date("w");
$today = date("Ymd");
$r = 45;
$cx = 50;
$cy = 50;
$g1 = 0;
$g2 = 0;
$x_leg = 10;
$y_leg = 110;
$w_h = 5;
$step = 10;

$img = ImageCreate(110,340);
$colorWhite = imageColorAllocate($img, 255,255,255);
$colorBlack = imageColorAllocate($img, 0,0,0);
$colorGrey = imageColorAllocate($img, 180,180,180);
for ($i=0;$i<6;$i++){
	imageArc($img, $cx, $cy, 2*$r+$i, 2*$r+$i, 0, 360, imageColorAllocate($img, 180+($i*10),180+($i*10),180+($i*10)));
}
imageFill($img, $cx, $cy, $colorGrey);

$color = array(
	imageColorAllocate($img, 237,27,35),
	imageColorAllocate($img, 255,172,72),
	imageColorAllocate($img, 56,181,73),
	imageColorAllocate($img, 0,174,239),
	imageColorAllocate($img, 255,0,255),
	imageColorAllocate($img, 255,242,0),
	imageColorAllocate($img, 0,0,255),
	imageColorAllocate($img, 166,124,81),
	imageColorAllocate($img, 183,183,183),
	imageColorAllocate($img, 0,255,0),
	imageColorAllocate($img, 255,0,0),
	imageColorAllocate($img, 0,90,100),
	imageColorAllocate($img, 0,255,153),
	imageColorAllocate($img, 153,0,153),
	imageColorAllocate($img, 153,204,0),
	imageColorAllocate($img, 153,153,255),
	imageColorAllocate($img, 255,104,208),
	imageColorAllocate($img, 17,126,27),
	imageColorAllocate($img, 217,9,107),
	imageColorAllocate($img, 51,214,81),
	imageColorAllocate($img, 239,235,122),
	imageColorAllocate($img, 105,105,94),
	imageColorAllocate($img, 231,186,29),
	imageColorAllocate($img, 221,217,213),
	imageColorAllocate($img, 146,11,129),
	imageColorAllocate($img, 10,148,205),
	imageColorAllocate($img, 139,248,185),
	imageColorAllocate($img, 156,140,6),
	imageColorAllocate($img, 196,85,247),
	imageColorAllocate($img, 238,147,158),
	imageColorAllocate($img, 231,134,29)
);
$tel = array(
	"MTN SA",
	"Orange RO",
	"Vodacom SA",
	"other SA",
	"Bharti IN",
	"CoteDIvoire",
	"Brunei tel",
	"Tmobile USA",
	"Argentina",
	"Bellsouth CO",
	"Jamaica",
	"Simpur Brunei",	
	"MTN SriLanka",
	"BITE LV",
	"Idea IN",
	"TELESTET",
	"Swipnet",
	"Infocom PH",
	"GRAMEENPHONEIT",
	"GPRSPune IN",
	"HUTCHVAS IN",
	"Planet PY",
	"Other"
); 
$result = mysqli_query($db, "SELECT `1`,`2`,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`17`,`18`,`19`,`20`,`21`,`22`,`Other` FROM `op` WHERE `uid`=$uid LIMIT 1");
$tel_data = mysqli_fetch_row($result);
if (empty($tel_data)) exit;
$result = mysqli_query($db, "SELECT `1`+`2`+`3`+`4`+`5`+`6`+`7`+`8`+`9`+`10`+`11`+`12`+`13`+`14`+`15`+`16`+`17`+`18`+`19`+`20`+`21`+`22`+`other` FROM `op` WHERE `uid`=$uid LIMIT 1");
$row = mysqli_fetch_row($result);
$sum_count = $row[0];
$percent_data = array();
$grad = array();
foreach ($tel_data as $index => $value)
{
	$percent_data[$index] = $value/$sum_count*100;
	$grad[$index] = $percent_data[$index]*360/100;
}
foreach ($grad as $index => $g)
{
	$g2 = $g1+$g;
	imageArc($img, $cx, $cy, 2*$r, 2*$r, $g1, $g2, $colorBlack);
	if ($g1 == 0){
		$x = $cx + $r;
		$y = $cy;
		imageLine($img,$cx,$cy,$x,$y,$colorBlack);
	}
	$x = $r*cos($g2*pi()/180)+$cx;
	$y = $r*sin($g2*pi()/180)+$cy;
	imageLine($img,$cx,$cy,$x,$y,$colorBlack);
	$x = ($r-5)*cos(($g2+$g1)/2*pi()/180)+$cx;
	$y = ($r-5)*sin(($g2+$g1)/2*pi()/180)+$cy;
	if ($g > 2){
		imageFill($img, $x, $y, $color[$index]);
	}
	$g1 += $g;
	imageFilledRectangle($img, $x_leg, $y_leg, $x_leg+$w_h, $y_leg+$w_h, $color[$index]);
	imageRectangle($img, $x_leg, $y_leg, $x_leg+$w_h, $y_leg+$w_h, $colorBlack);
	imageString($img, 1, $x_leg+$w_h+5, $y_leg+$w_h-6, $tel[$index]."(".(int)$percent_data[$index]."%)", $colorBlack);
	$y_leg += $step;
}
imageRectangle($img, 0, 105, 99, $y_leg, $colorBlack);
imageFilledRectangle($img, 0, 105, 5, $y_leg, $colorGrey);
imageRectangle($img, 0, 105, 5, $y_leg, $colorBlack);
Header("Content-type: image/png");
ImagePNG($img);
ImageDestroy($img);
?>