<?php
require_once "configtop.php";
define('FROOT',dirname(__FILE__));
//проверка  ввода id
if (empty($_GET['uid'])) exit;
$uid=intval($_GET['uid']);

// проверка существования учетной записи
$result = mysqli_query($db, "SELECT link FROM users WHERE uid=$uid");
$row=mysqli_fetch_row($result);
if (empty($row[0])) exit;

$hour_data = array();
$hour_host = array();

// текущий час, и дата
$hour = date("H");
$today = date("Ymd");

$temp_hour = $hour;
$temp_date = $today;
for ($i=0;$i<24;$i++)
{
	// данные за текущий час
	$result = mysqli_query($db, "SELECT count,host FROM count_24 WHERE hour=$temp_hour AND date=$temp_date AND uid=$uid ORDER BY id DESC LIMIT 1");
	$row=mysqli_fetch_row($result);
	
	// поверка наличия записи в базе
	if (empty($row[0]))
	{
		$hour_data[] = 0;
		$hour_host[] = 0;
	}else{
		$hour_data[] = $row[0];
		$hour_host[] = $row[1];
	}
	$temp_hour--;
	if ($temp_hour < 0)
	{
		$temp_hour = 23;
		$day = substr($temp_date,-2,2);
		if ($day-1 <= 0)
		{
			$month = substr($temp_date,-4,2);
			$temp_date = date("Ymd",mktime(0,0,0,$day-1,$month,date("Y")));
			
		} else $temp_date--;
	}
}
// максим посещаемость
$max = 0;
$max_index = 0;
foreach ($hour_data as $index => $value){
	if ($value > $max){
		$max = $value;
		$max_index = $index;
	}
}
//foreach ($hour_host as $value){
	//if ($value > $max) $max = $value;
//}
if ($max == 0) $max = 1;
// процентное соотношение хитов
$per_hit = array();
foreach ($hour_data as $value){
	$per_hit[] = $value*0.90/$max;
}
// процентное соотношение хостов
$per_host = array();
foreach ($hour_host as $value){
	$per_host[] = $value*0.90/$max;
}
$img = @imageCreateFromPNG("img/stat24.png");

// линейный 
$color1 = imageColorAllocate($img, 44,191,228);
$color2 = imageColorAllocate($img, 0,0,120);
$color_red = imageColorAllocate($img, 200,0,0);

$imageH = 96;
$imageW = 47;
$collW = 4;
$x1 = 101;
$y1 = (int)($imageW - $imageW*$per_hit[0] + 7);
$y1_host = (int)($imageW - $imageW*$per_host[0] + 7);
$x2 = $x1 - 3;
if ($hour_data[0] != 0 && $mark_graph && $i == $max_index){
	imageTTFtext($img, 6, 0, $x1-17, $y1-3, $color_red, FROOT."/font/font7.TTF","max");
	imageTTFtext($img, 6, 0, $x1+2, $y1-3, $color2, FROOT."/font/font7.TTF",$hour_data[0]);
	imageLine($img,$x1,$y1-7,$x1,$y1+42,$color_red);
}

for($i=1;$i<count($hour_data);$i++)
{
	// хиты
	$y2 = (int)($imageW - $imageW*$per_hit[$i] + 7);
	imageLine($img,$x1,$y1,$x2,$y2,$color1);
	
	// хосты 
	$y2_host = (int)($imageW - $imageW*$per_host[$i] + 7);
	imageLine($img,$x1,$y1_host,$x2,$y2_host,$color2);
	if ($hour_data[$i] != 0 && $mark_graph && $i == $max_index){
		imageTTFtext($img, 6, 0, $x2-17, $y2-3, $color_red, FROOT."/font/font7.TTF","max");
		imageTTFtext($img, 6, 0, $x2+2, $y2-3, $color2, FROOT."/font/font7.TTF",$hour_data[$i]);
		imageLine($img,$x2,$y2-7,$x2,$y2+42,$color_red);
	}
	$y1 = $y2;
	$y1_host = $y2_host;
	$x1 -= $collW;
	$x2 -= $collW;
}
Header("Content-type: image/png");
ImagePNG($img);
ImageDestroy($img);
?>
