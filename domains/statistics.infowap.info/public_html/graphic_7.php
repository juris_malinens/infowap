<?php
include "configtop.php";
define('FROOT',dirname(__FILE__));
//проверка  ввода id
if (empty($_GET['uid'])) exit;
$uid=intval($_GET['uid']);

// проверка существования учетной записи
$result = mysqli_query($db, "SELECT link FROM users WHERE uid=$uid");
$row=mysqli_fetch_row($result);
if (empty($row[0])) exit;

$week_data = array();

// день недели сегодня
$week_day = date("w");
// расчитываем дату обнуления
    $dayNext =$week_day + 7;
    if ($dayNext > 7){
        $dayNext-=7;
    }
    $cntDay=8-$dayNext;
    $nextMonday=date("Ymd",mktime(0,0,0,date("m"),date("d")+$cntDay));
// сегодня
$today = date("Ymd");
for ($i=1;$i<7;$i++)
{
	// данные за неделю
	$result = mysqli_query($db, "SELECT count FROM weeks WHERE next_mon=$nextMonday AND day_week=$i AND uid=$uid LIMIT 1");
	$row=mysqli_fetch_row($result);
	// поверка наличия записи в базе
	if (empty($row[0]))
	{
		$week_data[$i] = 0;
	}else $week_data[$i] = $row[0];
}
// данные за воскресенье
	$result = mysqli_query($db, "SELECT count FROM weeks WHERE next_mon=$nextMonday AND day_week=0 AND uid=$uid LIMIT 1");
	$row=mysqli_fetch_row($result);
	// поверка наличия записи в базе
	if (empty($row[0]))
	{
		$week_data[0] = 0;
	}else $week_data[0] = $row[0];
// максим посещаемость
$max = 0;
foreach ($week_data as $value){
	if ($value > $max) $max = $value;
}
if ($max == 0) $max = 1;
// процентное соотношение
$per_hit = array();
foreach ($week_data as $value){
	$per_hit[] = $value*0.90/$max;
}
// картинка
$img = @imageCreateFromPNG("img/stat7.png");
// столбиковый 
$imageH = 96;
$imageW = 47;
$collW = 10;

$x1 = 12;
$y2 = 59;
$x2 = $x1 + $collW;
$colorBlack = imageColorAllocate($img, 0,0,0);

foreach ($per_hit as $index=>$percent) {
	if ($index/2 == (int)($index/2)){
		$color = imageColorAllocate($img, 180,220,230);
		$color2 = imageColorAllocate($img, 152,200,212);
		$color3 = imageColorAllocate($img, 135,185,198);
	}
	else{
		$color = imageColorAllocate($img, 192,192,192);
		$color2 = imageColorAllocate($img, 180,180,180);
		$color3 = imageColorAllocate($img, 170,170,170);
	}
	$y1 = round($imageW - $imageW*$percent + 12);
	imageFilledRectangle($img,$x1,$y1,$x2,$y2,$color);
	$points = array(
	  0  => $x1,    // x1
	  1  => $y1,    // y1
	  2  => $x1+3,    // x2
	  3  => $y1-5,   // y2
	  4  => $x1+$collW+3,    // x3
	  5  => $y1-5,    // y3
	  6  => $x2,   // x4
	  7  => $y1,    // y4
	);
	imageFilledPolygon($img,$points,4,$color2);
	$points = array(
	  0  => $x2,    // x1
	  1  => $y1,    // y1
	  2  => $x1+$collW+3,    // x2
	  3  => $y1-5,   // y2
	  4  => $x1+$collW+3,    // x3
	  5  => $y2-5,    // y3
	  6  => $x2,   // x4
	  7  => $y2,    // y4
	);
	imageFilledPolygon($img,$points,4,$color3);
	$index_week = $index + 1;
	if ($index_week == 7) $index_week = 0;
	if ($week_data[$index_week] != 0 && $mark_graph) imageTTFtext($img, 6, 90, $x1+8 ,($y1+$y2)/2+(strlen($week_data[$index_week])*2), $colorBlack, FROOT."/font/font7.TTF",$week_data[$index_week]);
	$x1 += $collW;
	$x2 += $collW;
}
Header("Content-type: image/png");
ImagePNG($img);
ImageDestroy($img);
?>