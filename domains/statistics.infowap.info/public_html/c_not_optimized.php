<?php
date_default_timezone_set('Europe/Helsinki');
//$start_time = microtime(true);
if(!isset($_GET['uid']) OR empty($_GET['uid']) OR !is_numeric($_GET['uid'])) exit;
$c_c      = false;
$TimeZone = "+0";
$New_Time = time() + ($TimeZone * 60 * 60);

$banned = array(158,164,484,513,646,716,878,1016,1089,1112,
1161,1163,1207,1305,1322,1348,1400,1406,1413,1525,
1586,1661,1690,1756,1825,1981,2136,2149,2319,2393,
2463,2524,2600,2627,2659,2676,2751,2771,2781,2810,
2817,2864,3048,3119,3170,3309,3312,3379,3382,3384,
3416,3469,3574,3814,3835,3836,3840,3841,3946,4009,
4145,4157,4170,4183,4251,4306,4310,4311,4328,4354,
4359,4368,4381,4402,4428,4499,4501,4532,4542,4545,
4579,4632,4651,4669,4670,4671,4713,4724,4729,4737,
4760,4767,4770,4776,4779,4784,4788,4795,4798,4807,
4825,4826,4850,4881,4882,4883,4884,4885,4890,4898,
4900,4908,4923,4924,4932,4958,4983,5018,5021,5024,
5039,5039,5045,5072,5091,5117,5124,5135,5139,5140,5156,
5162,5238,5192,5253,5280,5301,5318,5321,5323,5324,5328,
5348,5366,5370,5371,5372,5373,5374,5402,5403,5438,
5446,5450,5455,5457,5475,5490,5513,5519,5523,5526,
5563,5566,5567,5597,5605,5607,5608,5609,5615,5615,
5627,5640,5752,5754,5755,5776,5790,5803,5813,5820,
5826,5845,5849,5900,5913,5921,5933,5940,5941,5942,
5976,5976,5986,6001);
$banned = array_flip($banned);
if (isset($banned[$_GET['uid']])) {
//if(in_array($_GET['uid'], $banned))
//{
  header("location: http://www.bildites.lv/images/0olajxwtnj9ojb1bn7t.png");
  exit;
}

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
include "configtop.php";
//include_once("proxy_detector.inc.php");
 $uid          = intval($_GET['uid']);
 $result       = mysqli_query($db, "SELECT SQL_CACHE link FROM users WHERE uid = '$uid'");
 $row          = mysqli_fetch_row($result);
if (empty($row)) exit;


 $ip           = $_SERVER['REMOTE_ADDR'];
 $newip        = sprintf("%u\n", ip2long($_SERVER['REMOTE_ADDR']));
 if(isset($_SERVER['HTTP_USER_AGENT']))
 $ua           = $_SERVER['HTTP_USER_AGENT'];
 else
 $ua           = "";
 $browser      = explode("/",$ua);
 $user_browser = $browser[0];
 $wid          = rand(1,6500);
 //$today        = date("Ymd",$New_Time); //DONT TOUCH!
 //$hour         = date("H",$New_Time);
 //$day          = date("d",$New_Time);
 $today        = date("Ymd");
 $hour         = date("H");
 $day          = date("d");
 
 //memcache start
 $memcache_obj = new Memcache;
 $memcache_obj->connect('127.0.0.1', 11211);


//every night cleanup
if(date("H")=="00" && date("i")=="02")
{
	 $memcache_obj->flush(); //memcache delete in midnight

	 mysqli_query($db, "TRUNCATE `hits_ipx3`");
	 mysqli_query($db, "UPDATE `continent` SET `AS` = '0', `SA` = '0', `AF` = '0', `NA` = '0', `EU` = '0', `OC` = '0' WHERE 1");
	 mysqli_query($db, "UPDATE `weeks2` SET `in` = 0, `out` = 0 WHERE 1");
	 mysqli_query($db, "UPDATE `country` SET 
	  `AD` = '0', `AE` = '0', `AF` = '0', `AG` = '0',
	  `AI` = '0',  `AL` = '0',  `AM` = '0',  `AN` = '0',  `AO` = '0',  `AP` = '0',  `AQ` = '0',  `AR` = '0',
	  `AS` = '0',  `AT` = '0',  `AU` = '0',  `AW` = '0',  `AZ` = '0',  `BA` = '0',  `BB` = '0',  `BD` = '0',
	  `BE` = '0',  `BF` = '0',  `BG` = '0',  `BH` = '0',  `BI` = '0',  `BJ` = '0',  `BM` = '0',  `BN` = '0',
	  `BO` = '0',  `BR` = '0',  `BS` = '0',  `BT` = '0',  `BV` = '0',  `BW` = '0',  `BY` = '0',  `BZ` = '0',
	  `CA` = '0',  `CC` = '0',  `CD` = '0',  `CF` = '0',  `CG` = '0',  `CH` = '0',  `CI` = '0',  `CK` = '0',
	  `CL` = '0',  `CM` = '0',  `CN` = '0',  `CO` = '0',  `CR` = '0',  `CU` = '0',  `CV` = '0',  `CX` = '0',
	  `CY` = '0',  `CZ` = '0',  `DE` = '0',  `DJ` = '0',  `DK` = '0',  `DM` = '0',  `DO` = '0',  `DZ` = '0',
	  `EC` = '0',  `EE` = '0',  `EG` = '0',  `EH` = '0',  `ER` = '0',  `ES` = '0',  `ET` = '0',  `EU` = '0',
	  `FI` = '0',  `FJ` = '0',  `FK` = '0',  `FM` = '0',  `FO` = '0',  `FR` = '0',  `FX` = '0',  `GA` = '0',
	  `GB` = '0',  `GD` = '0',  `GE` = '0',  `GF` = '0',  `GH` = '0',  `GI` = '0',  `GL` = '0',  `GM` = '0',
	  `GN` = '0',  `GP` = '0',  `GQ` = '0',  `GR` = '0',  `GS` = '0',  `GT` = '0',  `GU` = '0',  `GW` = '0',
	  `GY` = '0',  `HK` = '0',  `HM` = '0',  `HN` = '0',  `HR` = '0',  `HT` = '0',  `HU` = '0',  `ID` = '0',
	  `IE` = '0',  `IL` = '0',  `IN` = '0',  `IO` = '0',  `IQ` = '0',  `IR` = '0',  `IS` = '0',  `IT` = '0',
	  `JM` = '0',  `JO` = '0',  `JP` = '0',  `KE` = '0',  `KG` = '0',  `KH` = '0',  `KI` = '0',  `KM` = '0',
	  `KN` = '0',  `KP` = '0',  `KR` = '0',  `KW` = '0',  `KY` = '0',  `KZ` = '0',  `LA` = '0',  `LB` = '0',
	  `LC` = '0',  `LI` = '0',  `LK` = '0',  `LR` = '0',  `LS` = '0',  `LT` = '0',  `LU` = '0',  `LV` = '0',
	  `LY` = '0',  `MA` = '0',  `MC` = '0',  `MD` = '0',  `MG` = '0',  `MH` = '0',  `MK` = '0',  `ML` = '0',
	  `MM` = '0',  `MN` = '0',  `MO` = '0',  `MP` = '0',  `MQ` = '0',  `MR` = '0',  `MS` = '0',  `MT` = '0',
	  `MU` = '0',  `MV` = '0',  `MW` = '0',  `MX` = '0',  `MY` = '0',  `MZ` = '0',  `NA` = '0',  `NC` = '0',
	  `NE` = '0',  `NF` = '0',  `NG` = '0',  `NI` = '0',  `NL` = '0',  `NO` = '0',  `NP` = '0',  `NR` = '0',
	  `NU` = '0',  `NZ` = '0',  `OM` = '0',  `PA` = '0',  `PE` = '0',  `PF` = '0',  `PG` = '0',  `PH` = '0',
	  `PK` = '0',  `PL` = '0',  `PM` = '0',  `PN` = '0',  `PR` = '0',  `PS` = '0',  `PT` = '0',  `PW` = '0',
	  `PY` = '0',  `QA` = '0',  `RE` = '0',  `RO` = '0',  `RU` = '0',  `RW` = '0',  `SA` = '0',  `SB` = '0',
	  `SC` = '0',  `SD` = '0',  `SE` = '0',  `SG` = '0',  `SH` = '0',  `SI` = '0',  `SJ` = '0',  `SK` = '0',
	  `SL` = '0',  `SM` = '0',  `SN` = '0',  `SO` = '0',  `SR` = '0',  `ST` = '0',  `SV` = '0',  `SY` = '0',
	  `SZ` = '0',  `TC` = '0',  `TD` = '0',  `TF` = '0',  `TG` = '0',  `TH` = '0',  `TJ` = '0',  `TK` = '0',
	  `TM` = '0',  `TN` = '0',  `TO` = '0',  `TP` = '0',  `TR` = '0',  `TT` = '0',  `TV` = '0',  `TW` = '0',
	  `TZ` = '0',  `UA` = '0',  `UG` = '0',  `UM` = '0',  `US` = '0',  `UY` = '0',  `UZ` = '0',  `VA` = '0',
	  `VC` = '0',  `VE` = '0',  `VG` = '0',  `VI` = '0',  `VN` = '0',  `VU` = '0',  `WF` = '0',  `WS` = '0',
	  `YE` = '0',  `YT` = '0',  `YU` = '0',  `ZA` = '0',  `ZM` = '0',  `ZR` = '0',  `ZW` = '0'
	   WHERE 1");
   
}

 $result = mysqli_query($db, "SELECT `count`, `host` FROM `count_24` WHERE `uid` = '$uid' AND `date` = '$today' AND `hour` = '$hour' LIMIT 1");
 $row    = mysqli_fetch_row($result);
if (empty($row))
{
	$res        = mysqli_query($db, "SELECT COUNT(*) FROM `count_24` WHERE `uid` = '$uid'");
	$data       = mysqli_fetch_row($res);
	$count_rows = $data[0];
	if ($count_rows >= 24) mysqli_query($db, "DELETE FROM `count_24` WHERE `uid` = '$uid' ORDER BY `id` ASC LIMIT 1");
	mysqli_query($db, "INSERT INTO count_24 VALUES ('', $uid, $today, $hour, 0, 0);");
	$hour_count = 0;
	$hour_host  = 0;
}
 else
{
	$hour_count = $row[0];
	$hour_host  = $row[1];
}

 $hour_count++;
 //$today    = date("Ym",$New_Time); //DONT TOUCH!
 $today    = date("Ym");
 $result   = mysqli_query($db, "SELECT `count`, `host` FROM `months` WHERE `uid` = '$uid' AND `date` = '$today'");
 $row      = mysqli_fetch_row($result);

if (empty($row))
{
 mysqli_query($db, "INSERT INTO months VALUES ('', $uid, $today, 0, 0);");
 $this_month_count = 0;
 $this_month_host  = 0;
}
 else
{
 $this_month_count = $row[0];
 $this_month_host  = $row[1];
}
 $this_month_count++;
 //$today    = date("Ymd",$New_Time); //DONT TOUCH!
 //$week_day = date("w",$New_Time);
 $today    = date("Ymd"); //DONT TOUCH!
 $week_day = date("w");
 $result   = mysqli_query($db, "SELECT SQL_CACHE `next_mon` FROM `weeks` WHERE `uid` = '$uid' ORDER BY `id` ASC LIMIT 1");
 $row      = mysqli_fetch_row($result);
if (empty($row)) {
	$dayNext =$week_day + 7;
	
	if ($dayNext > 7)
		$dayNext-=7;
		
	$cntDay     = 8-$dayNext;
	$nextMonday = date("Ymd",$New_Time,mktime(0,0,0,date("m",$New_Time),date("d",$New_Time)+$cntDay));
} else
	$nextMonday = $row[0];

if ($today >= $nextMonday) {
	$dayNext =$week_day + 7;
	if ($dayNext > 7)
		$dayNext-=7;

	$cntDay         = 8-$dayNext;
	$nextMonday     = date("Ymd",mktime(0,0,0,date("m",$New_Time),date("d",$New_Time)+$cntDay));
	mysqli_query($db, "DELETE FROM weeks WHERE uid=$uid");
}
 $result         = mysqli_query($db, "SELECT `count`, `host` FROM `weeks` WHERE `uid` = '$uid' AND `date` = '$today' AND `day_week` = '$week_day' LIMIT 1");
 $row            = mysqli_fetch_row($result);
if (empty($row)) {
	$dayNext =$week_day + 7;
	if ($dayNext > 7)
		$dayNext-=7;

	$cntDay         = 8-$dayNext;
	$cntDay         = 8-$dayNext;
	$nextMonday     = date("Ymd",mktime(0,0,0,date("m",$New_Time),date("d",$New_Time)+$cntDay));
	mysqli_query($db, "INSERT INTO weeks VALUES ('',$uid,$nextMonday,$today,$week_day,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");
	$week_day_count = 0;
	$week_day_host  = 0;
} else {
	$week_day_count = $row[0];
	$week_day_host  = $row[1];
}
 $week_day_count++;

$unique = base64_encode(gzcompress($uid.$newip.$ua, 9));


if(!$memcache_obj->get($unique)) {
	$hour_host++;
	$week_day_host++;
	$this_month_host++;
	$memcache_obj->add($unique, '.', false, 0);
	
	$timez = date("G");
	if($timez > 1 && $timez < 6) {
		mysqli_query($db, "UPDATE `continent` SET ".$_SERVER["GEOIP_CONTINENT_CODE"]." = ".$_SERVER["GEOIP_CONTINENT_CODE"]."+1 WHERE `site` = '$uid'");
		mysqli_query($db, "UPDATE `country` SET ".$_SERVER["GEOIP_COUNTRY_CODE"]." = ".$_SERVER["GEOIP_COUNTRY_CODE"]."+1 WHERE `site` = '$uid'");
	}
}


 mysqli_query($db, "UPDATE `count_24` SET `count` = '$hour_count', `host` = '$hour_host' WHERE `uid` = '$uid' AND `date` = '$today' AND `hour` = '$hour'");
 mysqli_query($db, "UPDATE `weeks` SET `count` = '$week_day_count', `host` = '$week_day_host' WHERE `uid` = '$uid' AND `date` = '$today' AND `day_week` = '$week_day' LIMIT 1");
 $today       = date("Ym",$New_Time);
 mysqli_query($db, "UPDATE `months` SET `count` = '$this_month_count', `host` = '$this_month_host' WHERE `uid` = '$uid' AND `date` = '$today'");
 $count_today = $week_day_count;
 $host_today  = $week_day_host;
 $today       = date("Ymd",$New_Time);
 $now         = date("ymdHi");
 

 $del_time    = date("ymdHi", mktime(date("H"), date("i")-$online_time));

 mysqli_query($db, "DELETE FROM `online_ip` WHERE `uid`= '$uid' AND `time` <= '$del_time'");


//memcache online
$unique = base64_encode("online".$uid.$user_browser.$newip);

if(!$memcache_obj->get($unique)) {
	$memcache_obj->add($unique, '.', false, 900);
	$s = "INSERT INTO `online_ip` VALUES ('$uid','$ip','$now','$user_browser')";
	mysqli_query($db, $s);
	//file_put_contents('logs/online_ip_query_log.log', $s, FILE_APPEND);
	$result      = mysqli_query($db, "SELECT COUNT(ip) FROM `online_ip` WHERE `uid` = '$uid'");
	$row         = mysqli_fetch_row($result);
	$online      = $row[0];
} else {
	$result      = mysqli_query($db, "SELECT COUNT(ip) FROM `online_ip` WHERE `uid` = '$uid'");
	$row         = mysqli_fetch_row($result);
	$online      = $row[0];
}

 $today       = date("Ymd",$New_Time);
 $hour        = date("H",$New_Time);

 $result      = mysqli_query($db, "SELECT SUM(count) FROM `count_24` WHERE `uid` = '$uid' AND ((`date` = $today-1 AND `hour` > $hour) OR (`date` = $today AND `hour` <= $hour))");
 $row         = mysqli_fetch_row($result);
 $count_24    = $row[0];
 
 $result      = mysqli_query($db, "SELECT SUM(count) FROM `months` WHERE `uid` = '$uid'");
 $row         = mysqli_fetch_row($result);
 $all_count   = $row[0];

 
$f   = strlen($all_count);
$pos = 92-$f*7;

$f    = strlen($host_today);
$posh = 92-$f*7;

$f    = strlen($count_today);
$post = 92-$f*7;

$f    = strlen($online);
$poso = 92-$f*7;

$f    = strlen($count_24);
$pos4 = 92-$f*7;

ob_start();
$imgf = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imgf, 153,153,153);
imagestring($imgf, 1, 5, 4, "    statistics.", $color);
imagestring($imgf, 1, 5, 12, "   infowap.info", $color);
ImageGIF($imgf);
$out6 = ob_get_contents();
ob_end_clean();

ob_start();
$imga = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imga, 153,153,153);
imagestring($imga, 1, $pos+12, 8, $all_count, $color);
imagestring($imga, 1, 5, 8, "all hits",    $color);
ImageGIF($imga);
$out1 = ob_get_contents();
ob_end_clean();

ob_start();
$imgb = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imgb, 153,153,153);
imagestring($imgb, 1, $post+10, 8, $count_today, $color);
imagestring($imgb, 1, 5, 8, "today hits",      $color);
ImageGIF($imgb);
$out2 = ob_get_contents();
ob_end_clean();

ob_start();
$imgc = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imgc, 153,153,153);
imagestring($imgc, 1, $posh+10,  8, $host_today,  $color);
imagestring($imgc, 1, 5, 8, "today hosts",     $color);
ImageGIF($imgc);
$out3 = ob_get_contents();
ob_end_clean();

ob_start();
$imgd = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imgd, 153,153,153);
imagestring($imgd, 1, $posh+10, 8, $count_24, $color);
imagestring($imgd, 1, 5, 8, "24 hours",    $color);
ImageGIF($imgd);
$out4 = ob_get_contents();
ob_end_clean();

ob_start();
$imge = imagecreatetruecolor(100, 23);
$color  = imagecolorallocate($imge, 153,153,153);
imagestring($imge, 1, $poso, 8, $online, $color);
imagestring($imge, 1, 5, 8, "online",    $color);
ImageGIF($imge);
$out5 = ob_get_contents();
ob_end_clean();


include "GIFEncoder.class.php";

  $frames[] = $out6;
  $frames[] = $out1;
  $frames[] = $out2;
  $frames[] = $out3;
  $frames[] = $out4;
  $frames[] = $out5;
  $framed[] = 100;
  $framed[] = 100;
  $framed[] = 100;
  $framed[] = 100;
  $framed[] = 100;
  $framed[] = 100;
$gif = new GIFEncoder	(
							$frames,
							$framed,
							0,
							2,
							0, 0, 0,
							"bin"
		);
header('Content-type:image/gif');
echo $gif->GetAnimation();
//echo microtime(true) - $start_time;
exit;
?>